package lab.ddc.model;

import com.jfinal.plugin.activerecord.Model;

/**
 * 
 * @author zsc
 *
 */
public class League extends Model<League>{

	private static final long serialVersionUID = -2832993948417908134L;
	public static final League dao = new League();
	
}
