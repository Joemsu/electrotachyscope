<#ftl encoding='UTF-8'>
<#assign contextPath = ""/>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge"><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <title>品牌营销</title>
    <meta name="description" content="Form. Responsive HTML5 template.">
    <meta name="keywords" content="form, responsive, multi-purpose, portfolio, parallax, template, html5, agency">
    <meta name="author" content="Aether Themes">

    <!--<link rel="shortcut icon" href="img/favicon.png">-->
    <!--<link rel="apple-touch-icon" href="img/apple-touch-icon.png">-->
    <!--<link rel="apple-touch-icon" sizes="72x72" href="img/apple-touch-icon-72x72.png">-->
    <!--<link rel="apple-touch-icon" sizes="114x114" href="img/apple-touch-icon-114x114.png">-->

    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- jQuery -->
    <script src="js/jquery.js" type="text/javascript" charset="utf-8"></script>

    <!-- Stylesheets -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap3.css">
    <link rel="stylesheet" type="text/css" href="css/framework.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/job.css">
    <link rel="stylesheet" type="text/css" href="css/brand.css">
</head>
<script>
	$(function(){
		<#if tab==2>
			<#if (session.message)??>
				alert("${(session.message)!}");
			</#if>
		</#if>
	})
    function refushcode(){
        var d = new Date();
        document.getElementById("imgId").src="/randomCode?t="+d.toString(40);
    }
</script>
<body>
<!-- Begin Navigation -->

<nav class="desktop">
    <!-- Menu -->
    <ul class="nav-content clearfix">
        <li id="magic-line"></li>
        <li class="first upper">
            <a href="/index">网站首页</a>
        </li>
        <li class="drop upper">
            <a class="drop-btn" href="/about">关于钻豹</a>
            <ul class="drop-list">
                <li><a href="/about?tab=1">企业简介</a></li>
                <li><a href="/about?tab=2">总裁致辞</a></li>
                <li><a href="/about?tab=3">企业文化</a></li>
                <li><a href="/about?tab=4">企业荣誉</a></li>
                <li><a href="/about?tab=5">发展历程</a></li>
            </ul>
        </li>
        <li class="drop upper">
            <a class="drop-btn" href="/question">咨询中心</a>
            <ul class="drop-list">
                <li><a href="/question?tab=1">企业新闻</a></li>
                <li><a href="/question?tab=2">公司大事记</a></li>
                <li><a href="/question?tab=3">行业资讯</a></li>
            </ul>
        </li>
        <li class="upper"><a href="/product">产品中心</a></li>
        <li class="current-page drop upper">
            <a class="drop-btn" href="/brand">品牌营销</a>
            <ul class="drop-list">
                <li><a href="/brand?tab=1">营销网络</a></li>
                <li><a href="/brand?tab=2">加盟钻豹</a></li>
                <li><a href="/brand?tab=3">视频中心</a></li>
                <li><a href="/brand?tab=4">终端形象店</a></li>
                <li><a href="/brand?tab=5">终端活动</a></li>
            </ul>
        </li>
        <li class="drop upper">
            <a class="drop-btn" href="/servicecenter">服务中心</a>
            <ul class="drop-list">
                <li><a href="/servicecenter?tab=1">保养常识</a></li>
                <li><a href="/servicecenter?tab=2">售后服务</a></li>
                <li><a href="/servicecenter?tab=3">客户留言</a></li>
            </ul>
        </li>
        <li class="drop upper">
            <a class="drop-btn" href="/job">招贤纳士</a>
            <ul class="drop-list">
                <li><a href="/job?tab=1">用人理念</a></li>
                <li><a href="/job?tab=2">招聘流程</a></li>
                <li><a href="/job?tab=3">广纳贤才</a></li>
            </ul>
        </li>
        <li class="upper"><a href="/callus">联系钻豹</a></li>
    </ul><!-- END -->

</nav>
<header class="desktop">

    <!-- Logo -->
    <a href="index.html"><img class="logo" src="img/logo.png" alt="Form Logo" width="96" height="35" /></a>

    <!-- Menu Button -->
    <button type="button" class="nav-button">
        <div class="button-bars"></div>
    </button><!-- END -->

</header>

<div class="sticky-head"></div>
<!-- End Navigation -->
<!-- Begin Large Hero Block -->
<section class="hero accent parallax" ></section>
<!-- End Large Hero Block -->
<div id="container">
    <div id="wrapper">
        <!-- Left Menu Links -->
        <div id="smoothmenu1">
            <ul id="tab-menu" class="ddsmoothmenu" >
                <li <#if tab==1>class="selected"</#if>><a href="/brand?tab=1">营销网络</a></li>
                <li <#if tab==2>class="selected"</#if>><a href="/brand?tab=2">加盟钻豹</a></li>
                <li <#if tab==3>class="selected"</#if>><a href="/brand?tab=3">视频中心</a></li>
                <li <#if tab==4>class="selected"</#if>><a href="/brand?tab=4">终端形象店</a></li>
                <li <#if tab==5>class="selected"</#if>><a href="/brand?tab=5">终端活动</a></li>
            </ul><!-- Introduction Page Starts -->

            <div class="tab-content">
                <div class="tab <#if tab==1>show</#if>">
                    <h1>营销网络</h1>
                    <div class="net">
                       ${marketing.marketingnet!}
                    </div>
                </div><!-- Introduction Page Ends -->
                <!-- Resume Section Starts -->

                <div class="tab <#if tab==2>show</#if>">
                    <h1>加盟钻豹</h1>
                    <form class="form-horizontal" action="/league"  method="post">
                       <div class="form-group">
				<label class="col-md-3 control-label" for="company">公司名称:</label>
				<div class="col-md-7">
					<input type="text" id="company" name="league.company" class="form-control" placeholder="请输入公司名" required/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="person">留言人：</label>
				<div class="col-md-7">
					<input type="text" id="person" name="league.person" class="form-control" placeholder="请输入留言人" required/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="gender">性别</label>
				<div class="col-md-9">
					<select name="league.gender" id="gender">
						<option value="0">男</option>
						<option value="1">女</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="age">年龄：</label>
				<div class="col-md-7">
					<input type="number" id="age" name="league.age" class="form-control" placeholder="请输入年龄" max="100" min="10" required/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="region">所在地区：</label>
				<div class="col-md-7">
					<input type="text" id="region" name="league.region" class="form-control" placeholder="请输入所在地区" required/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="qq">QQ：</label>
				<div class="col-md-7">
					<input type="text" id="qq" name="league.qq" class="form-control" placeholder="请输入QQ" required/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="phone">联系电话：</label>
				<div class="col-md-7">
					<input type="text" id="phone" name="league.phone" class="form-control" placeholder="请输入联系电话" required/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="email">邮箱：</label>
				<div class="col-md-7">
					<input type="email" id="email" name="league.email" class="form-control" placeholder="请输入邮箱" required/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="fax">传真：</label>
				<div class="col-md-7">
					<input type="text" id="fax" name="league.fax" class="form-control" placeholder="请输入传真"/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="trade">从事行业：</label>
				<div class="col-md-9">
					<select name="league.trade" id="trade">
						<option value="0">电动车</option>
						<option value="1">其他</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="knowable">对电动车了解程度：</label>
				<div class="col-md-7">
					<select name="league.knowable" id="knowable">
						<option value="0">非常了解</option>
						<option value="1">比较了解</option>
						<option value="2">一般了解</option>
						<option value="3">不了解</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="hzintention">合作意向：</label>
				<div class="col-md-9">
					<select name="league.hzintention" id="hzintention">
						<option value="0">专营店（只代理钻豹品牌）</option>
						<option value="1">专营店</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="area">营业面积：</label>
				<div class="col-md-7">
					<select name="league.area" id="area">
						<option value="0">省会城市</option>
						<option value="1">150平米以上</option>
						<option value="2">地级市</option>
						<option value="3">120平米以上</option>
						<option value="4">县城</option>
						<option value="5">80平米以上</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="qtintention">洽谈意向：</label>
				<div class="col-md-7">
					<select name="league.qtintention" id="qtintention">
						<option value="0">到钻豹公司洽谈</option>
						<option value="1">区域经理联系您</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="goodbad">您认为钻豹电动车有什么优势和劣势？：</label>
				<div class="col-md-7">
					<textarea name="league.goodbad" id="goodbad" class="form-control" rows="5"></textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="brand">您所知道的前5个品牌？：</label>
				<div class="col-md-7">
					<textarea name="league.brand" id="brand" class="form-control" rows="5"></textarea>
				</div>
			</div>
			 <div class="form-group">
               <label class="col-md-3 control-label" for="code">验证码：</label>
                 <div class="col-md-9">
                <input name="randomCode" id="code" size="30" type="text" />
                 <img src="/randomCode" id="imgId" onclick="refushcode();" />
                 </div>
              </div>
			<div class="form-group">
				<div class="col-md-offset-2 col-md-10">
					<input type="submit" class="btn btn-primary" value="提交"/>
				</div>
                        </div>
                    </form>
                </div>

                <div class="tab <#if tab==3>show</#if>">
                    <h1>视频中心</h1>
                    <div class="video-box">
                        <embed src="${marketing.video!}" allowFullScreen="true" quality="high" width="100%" height="400" align="middle" allowScriptAccess="always" type="application/x-shockwave-flash"></embed>
                    </div>
                </div>
                <div class="tab <#if tab==4>show</#if>">
                    <h1>终端形象店</h1>
                    ${marketing.imageshop!}
                </div>
                <div class="tab <#if tab==5>show</#if>">
                    <h1>终端活动</h1>
                    ${marketing.imageactivity!}
                </div>

            </div>


            <div class="clearfix"></div>
        </div><!-- End -->
        <!-- TWITTER - ADD YOUR USERNAME IN ID INSTEAD OF MINE -->
        <div class="clear"></div>
    </div>
</div>

<!-- Begin Footer -->
<footer>
    <div class="container c-container">
        <div class="row">
            <!-- Social List -->
            <ul class="social-list">
                <li class="copyright">浙江钻豹电动车有限公司</li>
                <li class="copyright">页面版权所有 <a href="#" target="_blank"> 浙ICP备13001925号</a></li>
            </ul><!-- END -->
            <!-- Social List -->
            <ul class="social-list">
                <li class="copyright">WEBPAGE COPYRIGHT (C) 2012 WWW.WXZUANBAO.COM</li>
                <li class="copyright">友情链接： <a href="#" target="_blank"> 电动车</a></li>
                <li class="copyright"> <a href="admin" target="_blank">后台管理</a></li>
                <li class="copyright"> <a href="#" target="_blank"> 技术支持</a></li>
            </ul><!-- END -->
        </div>
    </div>
</footer>
<!-- End Footer -->

<!-- Javascript -->
<script src="js/backgroundcheck.js"></script>
<script src="js/plugins.js"></script>
<script src="js/main.js"></script>
<script type="text/javascript" src="js/scripts.js"></script>
<script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/jquery.validate.min.js"></script>
<script src="js/brand_form.js"></script>
<script type="text/javascript" charset="utf-8">
    MyValidator.init();
</script>
</body>

</html>