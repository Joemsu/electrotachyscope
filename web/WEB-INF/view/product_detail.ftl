<#ftl encoding='UTF-8'>
<#assign contextPath = ""/>
<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge"><![endif]-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<title>产品中心</title>
	<meta name="description" content="Form. Responsive HTML5 template.">
	<meta name="keywords" content="form, responsive, multi-purpose, portfolio, parallax, template, html5, agency">
	<meta name="author" content="Aether Themes">

	<!--<link rel="shortcut icon" href="img/favicon.png">-->
	<!--<link rel="apple-touch-icon" href="img/apple-touch-icon.png">-->
	<!--<link rel="apple-touch-icon" sizes="72x72" href="img/apple-touch-icon-72x72.png">-->
	<!--<link rel="apple-touch-icon" sizes="114x114" href="img/apple-touch-icon-114x114.png">-->

	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!-- jQuery -->
	<script src="js/jquery.js" type="text/javascript" charset="utf-8"></script>
	<!-- Stylesheets -->
	<link rel="stylesheet" type="text/css" href="css/framework.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/detail.css">

</head>

<body style="min-width:1200px;">
<nav class="desktop">
    <!-- Menu -->
    <ul class="nav-content clearfix">
        <li id="magic-line"></li>
        <li class="first upper">
            <a href="/index">网站首页</a>
        </li>
        <li class="drop upper">
            <a class="drop-btn" href="/about">关于钻豹</a>
            <ul class="drop-list">
                <li><a href="/about?tab=1">企业简介</a></li>
                <li><a href="/about?tab=2">总裁致辞</a></li>
                <li><a href="/about?tab=3">企业文化</a></li>
                <li><a href="/about?tab=4">企业荣誉</a></li>
                <li><a href="/about?tab=5">发展历程</a></li>
            </ul>
        </li>
        <li class="drop upper">
            <a class="drop-btn" href="/question">咨询中心</a>
            <ul class="drop-list">
                <li><a href="/question?tab=1">企业新闻</a></li>
                <li><a href="/question?tab=2">公司大事记</a></li>
                <li><a href="/question?tab=3">行业资讯</a></li>
            </ul>
        </li>
        <li class="current-page upper"><a href="/product">产品中心</a></li>
        <li class="drop upper">
            <a class="drop-btn" href="/brand">品牌营销</a>
            <ul class="drop-list">
                <li><a href="/brand?tab=1">营销网络</a></li>
                <li><a href="/brand?tab=2">加盟钻豹</a></li>
                <li><a href="/brand?tab=3">视频中心</a></li>
                <li><a href="/brand?tab=4">终端形象店</a></li>
                <li><a href="/brand?tab=5">终端活动</a></li>
            </ul>
        </li>
        <li class="drop upper">
            <a class="drop-btn" href="/servicecenter">服务中心</a>
            <ul class="drop-list">
                <li><a href="/servicecenter?tab=1">保养常识</a></li>
                <li><a href="/servicecenter?tab=2">售后服务</a></li>
                <li><a href="/servicecenter?tab=3">客户留言</a></li>
            </ul>
        </li>
        <li class="drop upper">
            <a class="drop-btn" href="/job">招贤纳士</a>
            <ul class="drop-list">
                <li><a href="/job?tab=1">用人理念</a></li>
                <li><a href="/job?tab=2">招聘流程</a></li>
                <li><a href="/job?tab=3">广纳贤才</a></li>
            </ul>
        </li>
        <li class="upper"><a href="/callus">联系钻豹</a></li>
    </ul><!-- END -->

</nav>
<header class="desktop">

    <!-- Logo -->
    <a href="/index"><img class="logo" src="img/logo.png" alt="Form Logo" width="96" height="35" /></a>

    <!-- Menu Button -->
    <button type="button" class="nav-button">
        <div class="button-bars"></div>
    </button><!-- END -->

</header>
<div class="sticky-head"></div>
<!-- End Navigation -->
<!-- Begin Large Hero Block -->
<section class="hero accent parallax" ></section>
<!-- End Large Hero Block -->


	<!-------------------------------------------------------------------------------->
	<div class="m-pro-detail">
		<div>
			<div class="mp-left">
				<div onmouseover="imgvisible();" onmouseout="imghidden();" id="magnifier">
					<img  onmouseover="imgvisible();" onmouseout="imghidden();" src="${product.coverphoto!}" id="img" />
					<div id="Browser"></div>
				</div>
				<div id="mag" style="visibility: hidden">
					<img id="magnifierImg" />
				</div>
                <br/>
				<a href="${"/product?page=1&tab="+product.type}">浏览同类产品</a><br/>
				<#if prenull == 1>
					<a href="#">上一页：无</a>
				<#else>
				<a class="pre-product" href="${"/product_detail?id="+pre.id!}">上一页：${pre.name!}</a>
				</#if>
				<#if nextnull == 1>
					<a href="#">下一页：无</a>
				<#else>
				<a href="${"/product_detail?id="+next.id!}">下一页：${next.name!}</a>
				</#if>
                <br/><br/>
			</div>
			<div class="mp-right">
				<div class="m-pro-detail-button">
					<b>产品详细介绍</b>
				</div>
				<hr/>
				<ul>
					<li>${product.detail!}</li>
				</ul>
				<div class="m-pro-detail-button m-button"><a href="#"><b>询价</b></a></div>
			</div>
            <p style="clear: both;"></p>

		</div>
		<div class="m-clear"></div>
	</div>
	<!-- END -->











<!-- Begin Footer -->
<footer>
    <div class="container c-container" style="margin-top: 0;">
        <div class="row">
            <!-- Social List -->
            <ul class="social-list">
                <li class="copyright">浙江钻豹电动车有限公司</li>
                <li class="copyright">页面版权所有 <a href="#" target="_blank"> 浙ICP备13001925号</a></li>
            </ul><!-- END -->
            <!-- Social List -->
            <ul class="social-list">
                <li class="copyright">WEBPAGE COPYRIGHT (C) 2012 WWW.WXZUANBAO.COM</li>
                <li class="copyright">友情链接： <a href="#" target="_blank"> 电动车</a></li>
                <li class="copyright"> <a href="admin" target="_blank">后台管理</a></li>
                <li class="copyright"> <a href="#" target="_blank"> 技术支持</a></li>
            </ul><!-- END -->
        </div>
    </div>
</footer>
<!-- End Footer -->

	<!-- Javascript -->
	<script src="js/backgroundcheck.js"></script>
	<script src="js/plugins.js"></script>
	<script src="js/main.js"></script>
<script src="js/showDetail.js"></script>
</body>

</html>