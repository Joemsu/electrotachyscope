package lab.ddc.model;

import com.jfinal.plugin.activerecord.Model;
/**
 * 
 * @author zkj
 *
 */
public class Article extends Model<Article> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5012370609028048805L;
	public static final Article dao = new Article();
}
