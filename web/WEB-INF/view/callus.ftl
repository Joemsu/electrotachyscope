<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge"><![endif]-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<title>联系我们</title>
	<meta name="description" content="Form. Responsive HTML5 template.">
	<meta name="keywords" content="form, responsive, multi-purpose, portfolio, parallax, template, html5, agency">
	<meta name="author" content="Aether Themes">

	<!--<link rel="shortcut icon" href="img/favicon.png">-->
	<!--<link rel="apple-touch-icon" href="img/apple-touch-icon.png">-->
	<!--<link rel="apple-touch-icon" sizes="72x72" href="img/apple-touch-icon-72x72.png">-->
	<!--<link rel="apple-touch-icon" sizes="114x114" href="img/apple-touch-icon-114x114.png">-->

	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!-- jQuery -->
	<script src="js/jquery.js" type="text/javascript" charset="utf-8"></script>

	<!-- Stylesheets -->
	<link rel="stylesheet" type="text/css" href="css/framework.css">

	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/callus.css">

</head>

<body>
<nav class="desktop">
    <!-- Menu -->
    <ul class="nav-content clearfix">
        <li id="magic-line"></li>
        <li class="first upper">
            <a href="/index">网站首页</a>
        </li>
        <li class="drop upper">
            <a class="drop-btn" href="/about">关于钻豹</a>
            <ul class="drop-list">
                <li><a href="/about?tab=1">企业简介</a></li>
                <li><a href="/about?tab=2">总裁致辞</a></li>
                <li><a href="/about?tab=3">企业文化</a></li>
                <li><a href="/about?tab=4">企业荣誉</a></li>
                <li><a href="/about?tab=5">发展历程</a></li>
            </ul>
        </li>
        <li class="drop upper">
            <a class="drop-btn" href="/question">咨询中心</a>
            <ul class="drop-list">
                <li><a href="/question?tab=1">企业新闻</a></li>
                <li><a href="/question?tab=2">公司大事记</a></li>
                <li><a href="/question?tab=3">行业资讯</a></li>
            </ul>
        </li>
        <li class="upper"><a href="/product">产品中心</a></li>
        <li class="drop upper">
            <a class="drop-btn" href="/brand">品牌营销</a>
            <ul class="drop-list">
                <li><a href="/brand?tab=1">营销网络</a></li>
                <li><a href="/brand?tab=2">加盟钻豹</a></li>
                <li><a href="/brand?tab=3">视频中心</a></li>
                <li><a href="/brand?tab=4">终端形象店</a></li>
                <li><a href="/brand?tab=5">终端活动</a></li>
            </ul>
        </li>
        <li class="drop upper">
            <a class="drop-btn" href="/servicecenter">服务中心</a>
            <ul class="drop-list">
                <li><a href="/servicecenter?tab=1">保养常识</a></li>
                <li><a href="/servicecenter?tab=2">售后服务</a></li>
                <li><a href="/servicecenter?tab=3">客户留言</a></li>
            </ul>
        </li>
        <li class="drop upper">
            <a class="drop-btn" href="/job">招贤纳士</a>
            <ul class="drop-list">
                <li><a href="/job?tab=1">用人理念</a></li>
                <li><a href="/job?tab=2">招聘流程</a></li>
                <li><a href="/job?tab=3">广纳贤才</a></li>
            </ul>
        </li>
        <li class="current-page upper"><a href="/callus">联系钻豹</a></li>
    </ul><!-- END -->

</nav>
<header class="desktop">

    <!-- Logo -->
    <a href="/"><img class="logo" src="img/logo.png" alt="Form Logo" width="96" height="35" /></a>

    <!-- Menu Button -->
    <button type="button" class="nav-button">
        <div class="button-bars"></div>
    </button><!-- END -->

</header>
<div class="sticky-head"></div>
<!-- End Navigation -->
<!-- Begin Large Hero Block -->
<section class="hero accent parallax" ></section>
<!-- End Large Hero Block -->


	<!-------------------------------------------------------------------------------->
	<div class="m-contact">
		<div class="sidebar">
			<!-- categories widget -->
			<div class="widget m-callus1">
				<h5>浙江钻豹电动车有限公司</h5>
				<ul>
					<li class="cat-item"><span>地　　　址：</span> 浙江省台州市椒江区农场大道（东联线）78号</li>
					<li class="cat-item"><span>邮　　　编：</span> 318000</li>
					<li class="cat-item"><span>行政部电话：</span> 0576-88127769</li>
					<li class="cat-item"><span>行政部传真：</span> 0576-88127765</li>
					<li class="cat-item"><span>客户服务部：</span> 0576-88122180（传真）</li>
					<li class="cat-item"><span>售后服务部：</span> 0576-88133812（传真）</li>
					<li class="cat-item"><span>邮　　　件：</span> <a style="color:black;" href="mailto:wxzbsh@126.com">wxzbsh@126.com</a></li>
				</ul>
			</div><!-- END -->
			<!-- categories widget -->
            <div class="m-right">
			<div class="widget m-callus2">
				<h5>河南钻豹车业有限公司</h5>
				<ul>
					<li class="cat-item"><span>地　　址：</span> 河南开封市通许县行政路</li>
					<li class="cat-item"><span>客服热线：</span> 0371-22205763/0371-22205788</li>
				</ul>
			</div><!-- END -->

			<div class="widget m-callus3">
				<h5>更多信息，请扫码</h5>
				<img style="width: 100px;" src="img/er.jpg"/>
			</div><!-- END -->
            </div>
		</div>
	</div>
	<!-- END -->





	<!-- Begin Testimonials Block -->
	<section class="content container m-callus-banner">
		<!-- Title -->
		<div class="row">
			<div class="title center col-sm-12">
				<h2>联系我们</h2>
				<p>如果您有任何的疑问，欢迎与我们取得联系，我们会尽快处理！</p>
				<div class="testimonial-pager"></div>
			</div>
		</div><!-- END -->
		<!-- Testimonials -->
		<div class="row">
			<div class="col-sm-12 animated fade" data-appear-bottom-offset="100">
				<ul class="testimonials">
					<li><p class="testimonial">售后服务部服务热线：400-8832-111，传真：0576-88133812</p><p class="highlighted">用我们的真诚微笑</p></li>
					<li><p class="testimonial">电子邮箱：wxzbsh@126.com</p><p class="highlighted">换取您对我们服务的满意</p></li>
					<li><p class="testimonial">地址：浙江省台州市椒江区农场大道（东联线）78号</p><p class="highlighted">用我们的真诚微笑</p></li>
					<li><p class="testimonial">用我们的真诚微笑，换取您对我们服务的满意</p><p class="highlighted">换取您对我们服务的满意</p></li>
				</ul>
			</div>
		</div><!-- END -->

	</section>
	<!-- End Testimonials Block -->

<!-- Begin Footer -->
<footer>
    <div class="container c-container">
        <div class="row">
            <!-- Social List -->
            <ul class="social-list">
                <li class="copyright">浙江钻豹电动车有限公司</li>
                <li class="copyright">页面版权所有 <a href="#" target="_blank"> 浙ICP备13001925号</a></li>
            </ul><!-- END -->
            <!-- Social List -->
            <ul class="social-list">
                <li class="copyright">WEBPAGE COPYRIGHT (C) 2012 WWW.WXZUANBAO.COM</li>
                <li class="copyright">友情链接： <a href="#" target="_blank"> 电动车</a></li>
                <li class="copyright"> <a href="admin" target="_blank">后台管理</a></li>
                <li class="copyright"> <a href="#" target="_blank"> 技术支持</a></li>
            </ul><!-- END -->
        </div>
    </div>
</footer>
<!-- End Footer -->

	<!-- Javascript -->
	<script src="js/backgroundcheck.js"></script>
	<script src="js/plugins.js"></script>
	<script src="js/main.js"></script>
</body>

</html>