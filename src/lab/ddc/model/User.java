package lab.ddc.model;

import com.jfinal.plugin.activerecord.Model;

/**
 * 
 * @author zsc
 *
 */
public class User extends Model<User> {

	private static final long serialVersionUID = 6740535037320251084L;
	public static final User dao = new User();
}
