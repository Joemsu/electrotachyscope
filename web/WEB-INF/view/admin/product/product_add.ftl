<#ftl encoding='UTF-8'>
<div class="span9">
	<form id="add-form" action=${baseUrl+"/add"} method="post" class="form-horizontal well">
		<fieldset>
			<legend>新增产品</legend>
			<input type="hidden" id="coverphoto" form="add-form" name="product.coverphoto" />
			<div class="control-group">
				<label class="control-label" for="name">产品名称：</label>
				<div class="controls">
					<input type="text" id="name"  form="add-form" name="product.name" class="span2" placeholder="请输入产品名字" required/>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="number">产品编号：</label>
				<div class="controls">
					<input type="text" id="number"  form="add-form" name="product.number" class="span2" placeholder="请输入产品编号" required/>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="type">类型：</label>
				<div class="controls">
					<select name = "product.type" id="type" form="add-form">
						<option value = "1">新品推荐</option>
						<option value = "2">新车速递</option>
						<option value = "3">豪华款</option>
						<option value = "4">简易款</option>
						<option value = "5">酷车款</option>
						<option value = "6">载重款</option>
						<option value = "7">电动三轮车</option>
					</select>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="imginput">图片：</label>
				<div class="controls">
					<input name="img" type="file" form="uploadForm" id="imginput"/>
					<div id="ajaxresult"
						style="width: 100px;height:100px;border: 1px solid #CCC;margin-top: 5px;">
					</div>
				</div>
			</div>
			<div style="control-group">
				<label class="control-label" for="detail">详情：</label>
				<div class="controls">
					<textarea id="detail" name="product.detail" form="add-form" style="width:100%;height:200px;"></textarea>
				</div>
			</div>
			
			<div class="control-group">
				<div class="controls">
					<input type="submit" class="btn btn-primary" value="提交"/>
				</div>
			</div>	
		</fieldset>
	</form>
</div>
<form id="uploadForm" class="form-horizontal well" enctype="multipart/form-data" style="display:none"></form>
<script type="text/javascript">
	$(function() {
		$("#imginput").on("change", function() {
			$("#uploadForm").ajaxSubmit({
				url : "http://120.26.82.151:8080/ddc/admin/product/ajaxupload",
				type : "post",
				dataType : "json",
				success : function(data) {
					$("#ajaxresult").html("");
					var img = new Image();
					img.src = data.imgurl;
					img.width = 100;
					img.height = 100;
					$("#coverphoto").attr("value",img.src);
					$("#ajaxresult").append(img);
				}
			});
		});
	});
</script>