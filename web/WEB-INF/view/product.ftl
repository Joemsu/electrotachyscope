<#ftl encoding='UTF-8'>
<#assign contextPath = ""/>
<!doctype html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge"><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <title>产品详情</title>
    <meta name="description" content="Form. Responsive HTML5 template.">
    <meta name="keywords" content="form, responsive, multi-purpose, portfolio, parallax, template, html5, agency">
    <meta name="author" content="Aether Themes">

    <!--<link rel="shortcut icon" href="img/favicon.png">-->
    <!--<link rel="apple-touch-icon" href="img/apple-touch-icon.png">-->
    <!--<link rel="apple-touch-icon" sizes="72x72" href="img/apple-touch-icon-72x72.png">-->
    <!--<link rel="apple-touch-icon" sizes="114x114" href="img/apple-touch-icon-114x114.png">-->

    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- jQuery -->
    <script src="js/jquery.js" type="text/javascript" charset="utf-8"></script>

    <!-- Stylesheets -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap3.css">
    <link rel="stylesheet" type="text/css" href="css/framework.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/job.css">
    <link rel="stylesheet" type="text/css" href="css/product.css">
</head>

<body>
<!-- Begin Navigation -->

<nav class="desktop">
    <!-- Menu -->
    <ul class="nav-content clearfix">
        <li id="magic-line"></li>
        <li class="first upper">
            <a href="/index">网站首页</a>
        </li>
        <li class="drop upper">
            <a class="drop-btn" href="/about">关于钻豹</a>
            <ul class="drop-list">
                <li><a href="/about?tab=1">企业简介</a></li>
                <li><a href="/about?tab=2">总裁致辞</a></li>
                <li><a href="/about?tab=3">企业文化</a></li>
                <li><a href="/about?tab=4">企业荣誉</a></li>
                <li><a href="/about?tab=5">发展历程</a></li>
            </ul>
        </li>
        <li class="drop upper">
            <a class="drop-btn" href="/question">咨询中心</a>
            <ul class="drop-list">
                <li><a href="/question?tab=1">企业新闻</a></li>
                <li><a href="/question?tab=2">公司大事记</a></li>
                <li><a href="/question?tab=3">行业资讯</a></li>
            </ul>
        </li>
        <li class="current-page upper"><a href="/product">产品中心</a></li>
        <li class="drop upper">
            <a class="drop-btn" href="/brand">品牌营销</a>
            <ul class="drop-list">
                <li><a href="/brand?tab=1">营销网络</a></li>
                <li><a href="/brand?tab=2">加盟钻豹</a></li>
                <li><a href="/brand?tab=3">视频中心</a></li>
                <li><a href="/brand?tab=4">终端形象店</a></li>
                <li><a href="/brand?tab=5">终端活动</a></li>
            </ul>
        </li>
        <li class="drop upper">
            <a class="drop-btn" href="/servicecenter">服务中心</a>
            <ul class="drop-list">
                <li><a href="/servicecenter?tab=1">保养常识</a></li>
                <li><a href="/servicecenter?tab=2">售后服务</a></li>
                <li><a href="/servicecenter?tab=3">客户留言</a></li>
            </ul>
        </li>
        <li class="drop upper">
            <a class="drop-btn" href="/job">招贤纳士</a>
            <ul class="drop-list">
                <li><a href="/job?tab=1">用人理念</a></li>
                <li><a href="/job?tab=2">招聘流程</a></li>
                <li><a href="/job?tab=3">广纳贤才</a></li>
            </ul>
        </li>
        <li class="upper"><a href="/callus">联系钻豹</a></li>
    </ul><!-- END -->

</nav>
<header class="desktop">

    <!-- Logo -->
    <a href="index.html"><img class="logo" src="img/logo.png" alt="Form Logo" width="96" height="35" /></a>

    <!-- Menu Button -->
    <button type="button" class="nav-button">
        <div class="button-bars"></div>
    </button><!-- END -->

</header>

<div class="sticky-head"></div>
<!-- End Navigation -->
<!-- Begin Large Hero Block -->
<section class="hero accent parallax" ></section>
<!-- End Large Hero Block -->
<div id="container">
    <div id="wrapper">
        <!-- Left Menu Links -->
        <div id="smoothmenu1">
            <ul id="tab-menu product-tab-menu" class="ddsmoothmenu" >
                <li <#if tab==1>class="selected"</#if>"><a href="/product?tab=1">新品推荐</a></li>
                <li <#if tab==2>class="selected"</#if>><a href="/product?tab=2">新车速递</a></li>
                <li <#if tab==3>class="selected"</#if>><a href="/product?tab=3">豪华款</a></li>
				<li <#if tab==4>class="selected"</#if>><a href="/product?tab=4">简易款</a></li>
				<li <#if tab==5>class="selected"</#if>><a href="/product?tab=5">载重款</a></li>
				<li <#if tab==6>class="selected"</#if>><a href="/product?tab=6">酷车款</a></li>
                <li <#if tab==7>class="selected"</#if>><a href="/product?tab=7">电动三轮车</a></li>
            </ul><!-- Introduction Page Starts -->

            <div class="tab-content" style="line-height:30px;text-indent:2em;">
                <div class="tab <#if tab==1>show</#if> ">
                    <h1>新品推荐</h1>
                     <div class="m-proc">
                     	<#list page.list as product>
                        <ul>
                            <li><a href="${"/product_detail?id="+product.id}"><img src="${product.coverphoto!}"/></a></li>
                            <li><a href="${"/product_detail?id="+product.id}">${product.name!}</a></li>
                        </ul>
                        </#list>
                    </div>
                      <nav>
                        <ul class="pagination m-nav pagination-sm">
                           <li>
							<a href="/product?tab=1&page=1">首页</a>
						</li>
						<#assign last = page.getPageNumber()-1/>
						<#assign current = page.getPageNumber()/>
						<#assign next = page.getPageNumber()+1/>
						<#if last gt 0>
							<li>
								<a href=${"/product?tab=1&page="+last}>上一页</a>
							</li>
						<#else>
							<li class="disabled">
								<a href="#">上一页</a>
							</li>
						</#if>
						<#list last-2..next+2 as n>
							<#if n gt 0&&n lte page.getTotalPage()>
								<#if n == current>
									<li class="active">
										<a href=${"/product?tab=1&page="+n}>${n}</a>
									</li>
								<#else>
									<li>
										<a href=${"/product?tab=1&page="+n}>${n}</a>
									</li>
								</#if>
							</#if>
						</#list>
						<#if next lte page.getTotalPage()>
							<li>
								<a href=${"/product?tab=1&page="+next}>下一页</a>
							</li>
						<#else>
							<li class="disabled">
								<a href="#">下一页</a>
							</li>
						</#if>
						<li>
							<a href=${"/product?tab=1&page="+page.getTotalPage()}>末页</a>
						</li>
                        </ul>
                    </nav>
                </div>

                <div class="tab <#if tab==2>show</#if>">
                    <h1>新车速递</h1>
                    <div class="m-proc">
                       <#list page.list as product>
                        <ul>
                            <li><a href="${"/product_detail?id="+product.id}"><img src="${product.coverphoto!}"/></a></li>
                            <li><a href="${"/product_detail?id="+product.id}">${product.name!}</a></li>
                        </ul>
                        </#list>
                    </div>
                    <nav>
                        <ul class="pagination m-nav pagination-sm">
                           <li>
							<a href="/product?tab=2&page=1">首页</a>
						</li>
						<#assign last = page.getPageNumber()-1/>
						<#assign current = page.getPageNumber()/>
						<#assign next = page.getPageNumber()+1/>
						<#if last gt 0>
							<li>
								<a href=${"/product?tab=2&page="+last}>上一页</a>
							</li>
						<#else>
							<li class="disabled">
								<a href="#">上一页</a>
							</li>
						</#if>
						<#list last-2..next+2 as n>
							<#if n gt 0&&n lte page.getTotalPage()>
								<#if n == current>
									<li class="active">
										<a href=${"/product?tab=2&page="+n}>${n}</a>
									</li>
								<#else>
									<li>
										<a href=${"/product?tab=2&page="+n}>${n}</a>
									</li>
								</#if>
							</#if>
						</#list>
						<#if next lte page.getTotalPage()>
							<li>
								<a href=${"/product?tab=2&page="+next}>下一页</a>
							</li>
						<#else>
							<li class="disabled">
								<a href="#">下一页</a>
							</li>
						</#if>
						<li>
							<a href=${"/product?tab=2&page="+page.getTotalPage()}>末页</a>
						</li>
                        </ul>
                    </nav>

                </div>

                <div class="tab <#if tab==3>show</#if>">
                    <h1>豪华款</h1>
                    <div class="m-proc">
                       <#list page.list as product>
                        <ul>
                            <li><a href="${"/product_detail?id="+product.id}"><img src="${product.coverphoto!}"/></a></li>
                            <li><a href="${"/product_detail?id="+product.id}">${product.name!}</a></li>
                        </ul>
                        </#list>
                    </div>
                    <nav>
                       <ul class="pagination m-nav pagination-sm">
                           <li>
							<a href="/product?tab=3&page=1">首页</a>
						</li>
						<#assign last = page.getPageNumber()-1/>
						<#assign current = page.getPageNumber()/>
						<#assign next = page.getPageNumber()+1/>
						<#if last gt 0>
							<li>
								<a href=${"/product?tab=3&page="+last}>上一页</a>
							</li>
						<#else>
							<li class="disabled">
								<a href="#">上一页</a>
							</li>
						</#if>
						<#list last-2..next+2 as n>
							<#if n gt 0&&n lte page.getTotalPage()>
								<#if n == current>
									<li class="active">
										<a href=${"/product?tab=3&page="+n}>${n}</a>
									</li>
								<#else>
									<li>
										<a href=${"/product?tab=3&page="+n}>${n}</a>
									</li>
								</#if>
							</#if>
						</#list>
						<#if next lte page.getTotalPage()>
							<li>
								<a href=${"/product?tab=3&page="+next}>下一页</a>
							</li>
						<#else>
							<li class="disabled">
								<a href="#">下一页</a>
							</li>
						</#if>
						<li>
							<a href=${"/product?tab=3&page="+page.getTotalPage()}>末页</a>
						</li>
                        </ul>
                    </nav>
                </div>

                <div class="tab <#if tab==4>show</#if>">
                    <h1>简易款</h1>
                    <div class="m-proc">
                        <#list page.list as product>
                        <ul>
                            <li><a href="${"/product_detail?id="+product.id}"><img src="${product.coverphoto!}"/></a></li>
                            <li><a href="${"/product_detail?id="+product.id}">${product.name!}</a></li>
                        </ul>
                        </#list>
                    </div>
                    <nav>
                       <ul class="pagination m-nav pagination-sm">
                           <li>
							<a href="/product?tab=4&page=1">首页</a>
						</li>
						<#assign last = page.getPageNumber()-1/>
						<#assign current = page.getPageNumber()/>
						<#assign next = page.getPageNumber()+1/>
						<#if last gt 0>
							<li>
								<a href=${"/product?tab=4&page="+last}>上一页</a>
							</li>
						<#else>
							<li class="disabled">
								<a href="#">上一页</a>
							</li>
						</#if>
						<#list last-2..next+2 as n>
							<#if n gt 0&&n lte page.getTotalPage()>
								<#if n == current>
									<li class="active">
										<a href=${"/product?tab=4&page="+n}>${n}</a>
									</li>
								<#else>
									<li>
										<a href=${"/product?tab=4&page="+n}>${n}</a>
									</li>
								</#if>
							</#if>
						</#list>
						<#if next lte page.getTotalPage()>
							<li>
								<a href=${"/product?tab=4&page="+next}>下一页</a>
							</li>
						<#else>
							<li class="disabled">
								<a href="#">下一页</a>
							</li>
						</#if>
						<li>
							<a href=${"/product?tab=4&page="+page.getTotalPage()}>末页</a>
						</li>
                        </ul>
                    </nav>
                </div>

                <div class="tab <#if tab==5>show</#if>">
                    <h1>载重款</h1>
                    <div class="m-proc">
                      <#list page.list as product>
                        <ul>
                            <li><a href="${"/product_detail?id="+product.id}"><img src="${product.coverphoto!}"/></a></li>
                            <li><a href="${"/product_detail?id="+product.id}">${product.name!}</a></li>
                        </ul>
                        </#list>
                    </div>
                    <nav>
                      <ul class="pagination m-nav pagination-sm">
                           <li>
							<a href="/product?tab=5&page=1">首页</a>
						</li>
						<#assign last = page.getPageNumber()-1/>
						<#assign current = page.getPageNumber()/>
						<#assign next = page.getPageNumber()+1/>
						<#if last gt 0>
							<li>
								<a href=${"/product?tab=5&page="+last}>上一页</a>
							</li>
						<#else>
							<li class="disabled">
								<a href="#">上一页</a>
							</li>
						</#if>
						<#list last-2..next+2 as n>
							<#if n gt 0&&n lte page.getTotalPage()>
								<#if n == current>
									<li class="active">
										<a href=${"/product?tab=5&page="+n}>${n}</a>
									</li>
								<#else>
									<li>
										<a href=${"/product?tab=5&page="+n}>${n}</a>
									</li>
								</#if>
							</#if>
						</#list>
						<#if next lte page.getTotalPage()>
							<li>
								<a href=${"/product?tab=5&page="+next}>下一页</a>
							</li>
						<#else>
							<li class="disabled">
								<a href="#">下一页</a>
							</li>
						</#if>
						<li>
							<a href=${"/product?tab=5&page="+page.getTotalPage()}>末页</a>
						</li>
                        </ul>
                    </nav>
                </div>

                <div class="tab <#if tab==6>show</#if>">
                    <h1>酷车款</h1>
                    <div class="m-proc">
                       <#list page.list as product>
                        <ul>
                            <li><a href="${"/product_detail?id="+product.id}"><img src="${product.coverphoto!}"/></a></li>
                            <li><a href="${"/product_detail?id="+product.id}">${product.name!}</a></li>
                        </ul>
                        </#list>
                    </div>
                    <nav>
                        <ul class="pagination m-nav pagination-sm">
                           <li>
							<a href="/product?tab=6&page=1">首页</a>
						</li>
						<#assign last = page.getPageNumber()-1/>
						<#assign current = page.getPageNumber()/>
						<#assign next = page.getPageNumber()+1/>
						<#if last gt 0>
							<li>
								<a href=${"/product?tab=6&page="+last}>上一页</a>
							</li>
						<#else>
							<li class="disabled">
								<a href="#">上一页</a>
							</li>
						</#if>
						<#list last-2..next+2 as n>
							<#if n gt 0&&n lte page.getTotalPage()>
								<#if n == current>
									<li class="active">
										<a href=${"/product?tab=6&page="+n}>${n}</a>
									</li>
								<#else>
									<li>
										<a href=${"/product?tab=6&page="+n}>${n}</a>
									</li>
								</#if>
							</#if>
						</#list>
						<#if next lte page.getTotalPage()>
							<li>
								<a href=${"/product?tab=6&page="+next}>下一页</a>
							</li>
						<#else>
							<li class="disabled">
								<a href="#">下一页</a>
							</li>
						</#if>
						<li>
							<a href=${"/product?tab=6&page="+page.getTotalPage()}>末页</a>
						</li>
                        </ul>
                    </nav>
                </div>

                <div class="tab <#if tab==7>show</#if>">
                    <h1>电动三轮车</h1>
                    <div class="m-proc">
                        <#list page.list as product>
                        <ul>
                            <li><a href="${"/product_detail?id="+product.id}"><img src="${product.coverphoto!}"/></a></li>
                            <li><a href="${"/product_detail?id="+product.id}">${product.name!}</a></li>
                        </ul>
                        </#list>
                    </div>
                    <nav>
                        <ul class="pagination m-nav pagination-sm">
                           <li>
							<a href="/product?tab=7&page=1">首页</a>
						</li>
						<#assign last = page.getPageNumber()-1/>
						<#assign current = page.getPageNumber()/>
						<#assign next = page.getPageNumber()+1/>
						<#if last gt 0>
							<li>
								<a href=${"/product?tab=7&page="+last}>上一页</a>
							</li>
						<#else>
							<li class="disabled">
								<a href="#">上一页</a>
							</li>
						</#if>
						<#list last-2..next+2 as n>
							<#if n gt 0&&n lte page.getTotalPage()>
								<#if n == current>
									<li class="active">
										<a href=${"/product?tab=7&page="+n}>${n}</a>
									</li>
								<#else>
									<li>
										<a href=${"/product?tab=7&page="+n}>${n}</a>
									</li>
								</#if>
							</#if>
						</#list>
						<#if next lte page.getTotalPage()>
							<li>
								<a href=${"/product?tab=7&page="+next}>下一页</a>
							</li>
						<#else>
							<li class="disabled">
								<a href="#">下一页</a>
							</li>
						</#if>
						<li>
							<a href=${"/product?tab=7&page="+page.getTotalPage()}>末页</a>
						</li>
                        </ul>
                    </nav>
                </div>

            </div>


            <div class="clearfix"></div>
        </div><!-- End -->
        <!-- TWITTER - ADD YOUR USERNAME IN ID INSTEAD OF MINE -->
        <div class="clear"></div>
    </div>
</div>

<!-- Begin Footer -->
<footer>
    <div class="container c-container">
        <div class="row">
            <!-- Social List -->
            <ul class="social-list">
                <li class="copyright">浙江钻豹电动车有限公司</li>
                <li class="copyright">页面版权所有 <a href="#" target="_blank"> 浙ICP备13001925号</a></li>
            </ul><!-- END -->
            <!-- Social List -->
            <ul class="social-list">
                <li class="copyright">WEBPAGE COPYRIGHT (C) 2012 WWW.WXZUANBAO.COM</li>
                <li class="copyright">友情链接： <a href="#" target="_blank"> 电动车</a></li>
                <li class="copyright"> <a href="admin" target="_blank">后台管理</a></li>
                <li class="copyright"> <a href="#" target="_blank"> 技术支持</a></li>
            </ul><!-- END -->
        </div>
    </div>
</footer>
<!-- End Footer -->

<!-- Javascript -->
<script src="js/backgroundcheck.js"></script>
<script src="js/plugins.js"></script>
<script src="js/main.js"></script>
<script type="text/javascript" src="js/scripts.js"></script>
<script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/jquery.validate.min.js"></script>
<script src="js/form.js"></script>
<script type="text/javascript" charset="utf-8">
    MyValidator.init();
</script>
</body>

</html>