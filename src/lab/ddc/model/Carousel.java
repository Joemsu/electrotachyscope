package lab.ddc.model;

import com.jfinal.plugin.activerecord.Model;

public class Carousel extends Model<Carousel> {

	private static final long serialVersionUID = 3543277014349097912L;
	public static final Carousel dao = new Carousel();
}
