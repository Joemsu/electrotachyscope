/**
 * 
 */
package lab.ddc.controller.admin;

import lab.ddc.model.Marketing;
import lab.ddc.util.Modelext;

import com.jfinal.core.Controller;
import com.jfinal.ext.route.ControllerBind;

/**
 * @author zkj
 *
 */
@ControllerBind(controllerKey="/admin/marketing",viewPath="/admin")
public class MarketingController extends Controller {
	
	private static final Modelext modelext = new Modelext("品牌营销","marketing");
	
	public void index(){
		setAttr("modelext", modelext);
		setAttr("marketing", Marketing.dao.findFirst("select * from marketing"));
		render("model.ftl");
	}
	
	public void add(){
		Marketing marketing = getModel(Marketing.class);
		marketing.update();
		redirect("/admin/marketing");
	}

}
