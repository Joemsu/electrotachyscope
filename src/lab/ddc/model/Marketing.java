/**
 * 
 */
package lab.ddc.model;

import com.jfinal.plugin.activerecord.Model;

/**
 * @author zkj
 *
 */
public class Marketing extends Model<Marketing> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2607737625267316055L;
	public static final Marketing dao = new Marketing();

}
