<#ftl encoding='UTF-8'>
<div class="span9">
	<h1>
		${modelext.getTitle()}管理
		<a href=${baseUrl+"/addform"} class="btn btn-default">新增${modelext.getTitle()}</a>
	</h1>
	<table class="table table-bordered table-striped">
		<thead>
			<tr>
				<th>
					序号
				</th>
				<th>
					产品名称
				</th>
				<th>
					产品编号
				</th>
				<th>
					上架时间
				</th>
				<th>
					浏览次数
				</th>
				<th>
					类型
				</th>
				<th>
					操作
				</th>
			</tr>
		</thead>
		<tbody>
			<#list page.list as product>
				<tr>
					<td>${product_index+1}</td>
					<td>${product.name!}</td>
					<td>${product.number!}</td>
					<td>${product.created!}</td>
					<td>${product.seetimes!}</td>
					<td>
						<#if product.type==1>
							新品推荐
						<#elseif product.type==2>
							新车速递
						<#elseif product.type==3>
							豪华款
						<#elseif product.type==4>
							简易款
						<#elseif product.type==5>
							酷车款
						<#elseif product.type==6>
							载重款
						<#elseif product.type==7>
							电动三轮车
						</#if>
					</td>
					<td>
						<a href="${baseUrl+"/updateform/"+product.id }" class="btn btn-primary">查看并更新</a> &nbsp;
						<a href="${baseUrl+"/delete/"+product.id}" class="btn btn-primary">刪除</a>
					</td>
				</tr>

			</#list>
		</tbody>
	</table>		
	<div class="pagination">
		<ul>
			<li>
				<a href=${baseUrl+"/1"}>首页</a>
			</li>
			<#assign last = page.getPageNumber()-1/>
			<#assign current = page.getPageNumber()/>
			<#assign next = page.getPageNumber()+1/>
			<#if last gt 0>
				<li>
					<a href=${baseUrl+"/"+last}>上一页</a>
				</li>
			<#else>
				<li class="disabled">
					<a href="#">上一页</a>
				</li>
			</#if>
			<#list last-2..next+2 as n>
				<#if n gt 0&&n lte page.getTotalPage()>
					<#if n == current>
						<li class="active">
							<a href=${baseUrl+"/"+n}>${n}</a>
						</li>
					<#else>
						<li>
							<a href=${baseUrl+"/"+n}>${n}</a>
						</li>
					</#if>
				</#if>
			</#list>
			<#if next lte page.getTotalPage()>
				<li>
					<a href=${baseUrl+"/"+next}>下一页</a>
				</li>
			<#else>
				<li class="disabled">
					<a href="#">下一页</a>
				</li>
			</#if>
			<li>
				<a href=${baseUrl+"/"+page.getTotalPage()}>末页</a>
			</li>
		</ul>
	</div>
</div>
