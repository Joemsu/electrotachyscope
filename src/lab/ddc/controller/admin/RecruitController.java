package lab.ddc.controller.admin;

import lab.ddc.model.Recruit;
import lab.ddc.util.Modelext;

import com.jfinal.core.Controller;
import com.jfinal.ext.route.ControllerBind;

@ControllerBind(controllerKey="/admin/recruit",viewPath="/admin")
public class RecruitController extends Controller {
	
	private static final Modelext modelext = new Modelext("招贤纳士","recruit");
	
	public void index(){
		setAttr("modelext", modelext);
		setAttr("recruit", Recruit.dao.findFirst("select * from recruit"));
		render("model.ftl");
	}
	
	public void add(){
		Recruit recruit = getModel(Recruit.class);
		recruit.update();
		redirect("/admin/recruit");
	}
}
