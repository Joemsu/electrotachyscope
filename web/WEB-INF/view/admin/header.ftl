<#ftl encoding='UTF-8'>
<div class="navbar">
    <div class="navbar-inner">
        <div class="container">
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </a> <a class="brand" href="/admin">电动车</a>
            <div class="nav-collapse">
                <ul class="nav">
                    <li class="active">
                        <a href="/admin">首页</a>
                    </li>
                </ul>
                <form class="navbar-search pull-left" action="#">
                    <select name="type" id="type" style="margin:0px;padding:0px;width:100px;">
                    	<option value="0">用户</option>
                    	<option value="1">文章</option>
                    	<option value="2">产品</option>
                    	<option value="3">留言</option>
                    	<option value="4">加盟</option>
                    </select>
                    <input type="text" class="search-query span2" name="search" placeholder="搜索" />
                </form>
                <ul class="nav pull-right">
                    <li>
                        <a href="#">欢迎：${(session.name)!}！</a>
                    </li>
                    <li>
                        <a href=${contextPath+"/logout"}>登出</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>