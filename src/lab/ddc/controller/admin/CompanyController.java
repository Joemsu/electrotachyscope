package lab.ddc.controller.admin;

import lab.ddc.model.Company;
import lab.ddc.util.Modelext;

import com.jfinal.core.Controller;
import com.jfinal.ext.route.ControllerBind;

@ControllerBind(controllerKey="/admin/company",viewPath="/admin")
public class CompanyController extends Controller {
	
	private static final Modelext modelext = new Modelext("公司简介","company");
	
	public void index(){
		setAttr("modelext", modelext);
		setAttr("company", Company.dao.findFirst("select * from company"));
		render("model.ftl");
	}
	
	public void add(){
		Company company = getModel(Company.class);
		company.update();
		redirect("/admin/company");
	}
}
