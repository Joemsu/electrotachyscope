/**
 * 
 */
package lab.ddc.controller.proscenium;

import lab.ddc.model.League;
import lab.ddc.model.Message;

import com.jfinal.core.Controller;
import com.jfinal.ext.render.CaptchaRender;
import com.jfinal.ext.route.ControllerBind;

/**
 * @author zkj
 *
 */
@ControllerBind(controllerKey="/league",viewPath="/")
public class LeagueController extends Controller {
	
	public void index(){
		String inputRandomCode = getPara("randomCode");
        boolean validate = CaptchaRender.validate(this, inputRandomCode, String.valueOf(getSessionAttr("time")));
		System.out.println("validate="+validate);
        if (validate){
			League league = getModel(League.class);
			league.save();
			setSessionAttr("message", "已提交成功，请勿再提交！");
			removeSessionAttr("time");
		}else{
			setSessionAttr("message", "验证码错误！");
		}
		redirect("/brand?tab=2");
	}

}
