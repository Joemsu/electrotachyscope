<#ftl encoding='UTF-8'>
<div class="span9">
	<form id="add-form" action=${baseUrl+"/add"} method="post" enctype="multipart/form-data" class="form-horizontal well">
		<fieldset>
			<legend>新增轮播图</legend>
			<div class="control-group">
				<label class="control-label" for="imginput">图片：</label>
				<div class="controls">
					<input type="file" name="img" id="imginput" required>
				</div>
			</div>	
			<div class="control-group">
				<div class="controls">
					<input type="submit" class="btn btn-primary" value="提交"/>
				</div>
			</div>	
		</fieldset>
	</form>
</div>