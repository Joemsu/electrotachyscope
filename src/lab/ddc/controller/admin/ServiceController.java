package lab.ddc.controller.admin;

import lab.ddc.model.Service;
import lab.ddc.util.Modelext;

import com.jfinal.core.Controller;
import com.jfinal.ext.route.ControllerBind;

@ControllerBind(controllerKey="/admin/service",viewPath="/admin")
public class ServiceController extends Controller {
	
	private static final Modelext modelext = new Modelext("服务中心","service");
	
	public void index(){
		setAttr("modelext", modelext);
		setAttr("service", Service.dao.findFirst("select * from service"));
		render("model.ftl");
	}
	
	public void add(){
		Service service = getModel(Service.class);
		service.update();
		redirect("/admin/service");
	}
}
