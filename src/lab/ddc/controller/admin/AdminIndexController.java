package lab.ddc.controller.admin;

import com.jfinal.core.Controller;
import com.jfinal.ext.route.ControllerBind;

@ControllerBind(controllerKey = "/admin",viewPath="/")
public class AdminIndexController extends Controller {
	
	public void index(){
	    if (getSessionAttr("name")==null)
	    	render("login.html");
	    else {
			render("admin/index.ftl");
		}
	}

}
