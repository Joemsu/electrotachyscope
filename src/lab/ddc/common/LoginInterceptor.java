package lab.ddc.common;

import com.jfinal.aop.Interceptor;
import com.jfinal.core.ActionInvocation;
import com.jfinal.core.Controller;

public class LoginInterceptor implements Interceptor {
	
	public void intercept(ActionInvocation ai) {
        Controller controller = ai.getController();
        String name = controller.getSessionAttr("name");
        if(name == null&&ai.getControllerKey().startsWith("/admin")&&!ai.getControllerKey().equals("/admin"))
        	controller.redirect("/admin");
        else {
        	ai.invoke();
        }
    }
}
