<#ftl encoding='UTF-8'>
<script type="text/javascript" src=${contextPath+"/ueditor/ueditor.config.js"}></script>
<script type="text/javascript" src=${contextPath+"/ueditor/ueditor.all.js"}></script> 
<script type="text/javascript" src=${contextPath+"/ueditor/lang/zh-cn/zh-cn.js"}></script> 
<div class="span9">
	<ul class="nav nav-tabs" id="myTab">
	  <li class="active"><a href="#intro" data-toggle="tab">企业简介</a></li>
	  <li><a href="#oration" data-toggle="tab">总裁致辞</a></li>
	  <li><a href="#culture" data-toggle="tab">企业文化</a></li>
	  <li><a href="#glory" data-toggle="tab">企业荣誉</a></li>
	  <li><a href="#history" data-toggle="tab">发展历程</a></li>
	</ul>
	<div class="tab-content">
	  <div class="tab-pane active" id="intro">
	  	<form action=${baseUrl+"/add"} method="post" class="form-horizontal well">
	  		<input type="hidden" name="company.id" value=${company.id}>
			<fieldset>
				<div style="control-group">
					<label class="control-label" for="intro1">内容：</label>
					<div class="controls">
						<script type="text/plain" id="intro1" name="company.intro" style="width:100%;height:500px;">${company.intro!}</script>
					</div>
				</div>
				<div class="control-group">
					<div class="controls">
						<input type="submit" class="btn btn-primary" value="提交"/>
					</div>
				</div>	
			</fieldset>
		</form>
	  </div>
	  <div class="tab-pane" id="oration">
	  	<form action=${baseUrl+"/add"} method="post" class="form-horizontal well">
	  		<input type="hidden" name="company.id" value=${company.id}>
			<fieldset>
				<div style="control-group">
					<label class="control-label" for="oration1">内容：</label>
					<div class="controls">
						<script type="text/plain" id="oration1" name="company.oration" style="width:100%;height:500px;">${company.oration!}</script>
					</div>
				</div>
				<div class="control-group">
					<div class="controls">
						<input type="submit" class="btn btn-primary" value="提交"/>
					</div>
				</div>	
			</fieldset>
		</form>
	  </div>
	  <div class="tab-pane" id="culture">
	  	<form action=${baseUrl+"/add"} method="post" class="form-horizontal well">
	  		<input type="hidden" name="company.id" value=${company.id}>
			<fieldset>
				<div style="control-group">
					<label class="control-label" for="culture1">内容：</label>
					<div class="controls">
						<script type="text/plain" id="culture1" name="company.culture" style="width:100%;height:500px;">${company.culture!}</script>
					</div>
				</div>
				<div class="control-group">
					<div class="controls">
						<input type="submit" class="btn btn-primary" value="提交"/>
					</div>
				</div>	
			</fieldset>
		</form>
	  </div>
	  <div class="tab-pane" id="glory">
	  	<form action=${baseUrl+"/add"} method="post" class="form-horizontal well">
	  		<input type="hidden" name="company.id" value=${company.id}>
			<fieldset>
				<div style="control-group">
					<label class="control-label" for="glory1">内容：</label>
					<div class="controls">
						<script type="text/plain" id="glory1" name="company.glory" style="width:100%;height:500px;">${company.glory!}</script>
					</div>
				</div>
				<div class="control-group">
					<div class="controls">
						<input type="submit" class="btn btn-primary" value="提交"/>
					</div>
				</div>	
			</fieldset>
		</form>
	  </div>
	  <div class="tab-pane" id="history">
	  	<form action=${baseUrl+"/add"} method="post" class="form-horizontal well">
	  		<input type="hidden" name="company.id" value=${company.id}>
			<fieldset>
				<div style="control-group">
					<label class="control-label" for="history1">内容：</label>
					<div class="controls">
						<script type="text/plain" id="history1" name="company.history" style="width:100%;height:500px;">${company.history!}</script>
					</div>
				</div>
				<div class="control-group">
					<div class="controls">
						<input type="submit" class="btn btn-primary" value="提交"/>
					</div>
				</div>	
			</fieldset>
		</form>
	  </div>
	</div>
	
</div>
<script type="text/javascript">
	UE.getEditor('intro1', {allowDivTransToP: false});
	UE.getEditor('oration1', {allowDivTransToP: false});
	UE.getEditor('culture1', {allowDivTransToP: false});
	UE.getEditor('glory1', {allowDivTransToP: false});
	UE.getEditor('history1', {allowDivTransToP: false});
</script>