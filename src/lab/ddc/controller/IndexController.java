package lab.ddc.controller;

import java.util.Date;

import lab.ddc.model.Article;
import lab.ddc.model.Carousel;
import lab.ddc.model.Company;
import lab.ddc.model.Marketing;
import lab.ddc.model.Product;
import lab.ddc.model.Recruit;
import lab.ddc.model.Service;

import com.jfinal.core.Controller;
import com.jfinal.ext.render.CaptchaRender;
import com.jfinal.ext.route.ControllerBind;

@ControllerBind(controllerKey = "/")
public class IndexController extends Controller {
	
	public void index(){
		setAttr("articles", Article.dao.paginate(1, 8, "select *", "from article where type= "+1+" order by id desc"));
	    setAttr("products", Product.dao.paginate(1, 8, "select *", "from product where type= "+1+" order by id desc"));
		setAttr("carousels", Carousel.dao.find("select * from carousel order by created desc limit 5"));
	    render("index.ftl");
	}
	
	
	public void question(){
		Integer tab = getParaToInt("tab");
		Integer id = getParaToInt("id");
		if (tab == null)
			tab = 1;
		setAttr("tab", tab);
		if(id!=null)
			setAttr("id", id);
		render("question.ftl");
	}
	
	public void about(){
		Integer tab = getParaToInt("tab");
		if (tab == null)
			tab = 1;
		setAttr("tab", tab);
		setAttr("company", Company.dao.findFirst("select * from company"));
		render("about.ftl");
	}
	
	public void job(){
		Integer tab = getParaToInt("tab");
		if (tab == null)
			tab = 1;
		setAttr("tab", tab);
		setAttr("recruit", Recruit.dao.findFirst("select * from recruit"));
		render("job.ftl");
	}
	
	public void product(){
		Integer tab = getParaToInt("tab");
		Integer page = 1;
        if (getParaToInt("page")!=null){
            page = getParaToInt("page");
        }
		if(tab == null){
			tab = 1;
		}
		setAttr("tab", tab);
		if(tab==1)
			setAttr("page", Product.dao.paginate(page, 6, "select *", "from product where type= "+1+" order by id desc"));
		if(tab==2)
			setAttr("page", Product.dao.paginate(page, 6, "select *", "from product where type= "+2+" order by id desc"));
		if(tab==3)
			setAttr("page", Product.dao.paginate(page, 6, "select *", "from product where type= "+3+" order by id desc"));
		if(tab==4)
			setAttr("page", Product.dao.paginate(page, 6, "select *", "from product where type= "+4+" order by id desc"));
		if(tab==5)
			setAttr("page", Product.dao.paginate(page, 6, "select *", "from product where type= "+5+" order by id desc"));
		if(tab==6)
			setAttr("page", Product.dao.paginate(page, 6, "select *", "from product where type= "+6+" order by id desc"));
		if(tab==7)
			setAttr("page", Product.dao.paginate(page, 6, "select *", "from product where type= "+7+" order by id desc"));
		render("product.ftl");
	}
	
	public void product_detail(){
		Integer id = getParaToInt("id");
		setAttr("product", Product.dao.findById(id));
		Product pre = new Product();
		Product next = new Product();
		if(Product.dao.findFirst("select * from product where id < "+id+" order by id desc")!=null){
			pre = Product.dao.findFirst("select * from product where id < "+id+" order by id desc");
			setAttr("prenull", 0);
		}
		else{
			setAttr("prenull", 1);
		}
		if(Product.dao.findFirst("select * from product where id >"+id)!=null){
			next = Product.dao.findFirst("select * from product where id >"+id);
			setAttr("nextnull", 0);
		}else{
			setAttr("nextnull", 1);
		}
		setAttr("pre",pre);
		setAttr("next",next);
		render("product_detail.ftl");
	}
	
	public void callus(){
		render("callus.ftl");
	}
	
	public void brand(){
		Integer tab = getParaToInt("tab");
		if (tab == null)
			tab = 1;
		setAttr("tab", tab);
		setAttr("marketing", Marketing.dao.findFirst("select * from marketing"));
		render("brand.ftl");
	}
	
	public void servicecenter(){
		Integer tab = getParaToInt("tab");
		if (tab == null)
			tab = 1;
		setAttr("tab", tab);
		setAttr("service", Service.dao.findFirst("select * from service"));
		render("service-center.ftl");
	}
	
	public void randomCode() {
		String time = String.valueOf(new Date().getTime()); 
		setSessionAttr("time", time);
	    render(new CaptchaRender(time));
	}
}
