<#ftl encoding='UTF-8'>
<#assign contextPath = ""/>
<#assign baseUrl = modelext.getBaseUrl()/>
<#assign modelName = modelext.getModelName()/>
<!DOCTYPE html>
<!--[if lt IE 7 ]><html lang="en" class="ie6 ielt7 ielt8 ielt9"><![endif]--><!--[if IE 7 ]><html lang="en" class="ie7 ielt8 ielt9"><![endif]--><!--[if IE 8 ]><html lang="en" class="ie8 ielt9"><![endif]--><!--[if IE 9 ]><html lang="en" class="ie9"> <![endif]--><!--[if (gt IE 9)|!(IE)]><!--> 
<html lang="en"><!--<![endif]--> 
    <head>
        <meta charset="utf-8">
        <title>更新${modelext.getTitle()}</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href=${contextPath+"/css/bootstrap.min.css"} rel="stylesheet">
        <link href=${contextPath+"/css/bootstrap-responsive.min.css"} rel="stylesheet">
        <link href=${contextPath+"/css/site.css"} rel="stylesheet">
        <script src=${contextPath+"/js/jquery.min.js"}></script>
        <script src=${contextPath+"/js/jquery-form.js"}></script>
    	<script src=${contextPath+"/js/bootstrap.min.js"}></script>
   		<script src=${contextPath+"/js/site.js"}></script>
        <!--[if lt IE 9]><script src=${contextPath+"js/html5.js"}></script><![endif]-->
    </head>
<body>
    <div class="container">
        <#include "./header.ftl">
        <div class="row">
        	<#include "./left.ftl">
			<#include modelName+"/"+modelName+"_update.ftl">
        </div>
    </div>
    <#include "./footer.ftl">
</body>
<html>