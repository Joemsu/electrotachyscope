package lab.ddc.controller.admin;

import lab.ddc.model.League;
import lab.ddc.util.Modelext;

import com.jfinal.core.Controller;
import com.jfinal.ext.route.ControllerBind;

@ControllerBind(controllerKey="/admin/league",viewPath="/admin")
public class LeagueController extends Controller{
	
private static final Modelext modelext = new Modelext("加盟","league");
	
	public void index(){
		int page = 1;
		if(getParaToInt()!=null){
			page = getParaToInt();
		}
		setAttr("modelext", modelext);
		setAttr("page", League.dao.paginate(page, 15, "select *", "from league"));
		render("model.ftl");
	}
	
	public void addform(){
		setAttr("modelext", modelext);
		render("model_add.ftl");
	}
	
	public void add(){
		League league = getModel(League.class);
		league.save();
		redirect("/admin/league");
	}
	
	public void delete(){
		League league = League.dao.findById(getParaToInt());
		league.delete();
		redirect("/admin/league");
		
	}
	
	public void updateform(){
		setAttr("modelext", modelext);
		setAttr("league", League.dao.findById(getParaToInt()));
		render("model_update.ftl");
	}
	
	public void update(){
		League league = getModel(League.class);
		league.update();
		redirect("/admin/league");
	}
}
