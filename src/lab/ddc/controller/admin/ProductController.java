/**
 * 
 */
package lab.ddc.controller.admin;

import lab.ddc.model.Product;
import lab.ddc.util.Modelext;

import com.jfinal.core.Controller;
import com.jfinal.ext.route.ControllerBind;
import com.jfinal.render.JsonRender;
import com.jfinal.upload.UploadFile;

/**
 * @author zkj
 *
 */
@ControllerBind(controllerKey="/admin/product",viewPath="/admin")
public class ProductController extends Controller {

	private static final Modelext modelext = new Modelext("产品","product");
	
	public void index(){
		int page = 1;
        if (getParaToInt()!=null){
            page = getParaToInt();
        }
		setAttr("modelext", modelext);
		setAttr("page", Product.dao.paginate(page, 15, "select * ", "from product"));
		render("model.ftl");
	}
	
	public void addform(){
		setAttr("modelext", modelext);
		render("model_add.ftl");
	}
	
	public void add(){
		Product product = getModel(Product.class);
		product.save();
		redirect("/admin/product");
	}
	
	public void updateform(){
		setAttr("modelext", modelext);
		setAttr("product", Product.dao.findById(getParaToInt()));
		render("model_update.ftl");
	}
	
	public void update(){
		Product product = getModel(Product.class);
		product.update();
		redirect("/admin/product");
	}
	
	public void delete(){
		Product product = Product.dao.findById(getParaToInt());
		product.delete();
		redirect("/admin/product");
	}
	
	public void ajaxupload(){
		UploadFile file=getFile("img");
		//这个虽然是forIE 但是其他浏览器也有效 ie也好用
		render(new JsonRender("imgurl","http://120.26.82.151:8080/ddc/upload/product/"+file.getFileName()).forIE());
	}
}
