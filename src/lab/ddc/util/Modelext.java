package lab.ddc.util;

public class Modelext{

	private String Title;
	private String ModelName;
	
	public Modelext(String title, String modelName) {
		super();
		Title = title;
		ModelName = modelName;
	}

	public Modelext() {
		super();
	}

	public String getTitle(){
		return Title;
	}
	
	public String getModelName(){
		return ModelName;
	}
	
	public String getBaseUrl(){
		return "http://120.26.82.151:8080/ddc/admin/"+ModelName;
	}
	
}

