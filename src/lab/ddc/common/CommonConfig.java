package lab.ddc.common;

import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.core.JFinal;
import com.jfinal.ext.interceptor.SessionInViewInterceptor;
import com.jfinal.ext.plugin.tablebind.AutoTableBindPlugin;
import com.jfinal.ext.plugin.tablebind.SimpleNameStyles;
import com.jfinal.ext.route.AutoBindRoutes;
import com.jfinal.kit.PathKit;
import com.jfinal.plugin.c3p0.C3p0Plugin;

public class CommonConfig extends JFinalConfig {
	/**
	 * 配置JFinal的常量值
	 */
	@Override
	public void configConstant(Constants me) {
		//加载少量必要配置，随后可用getProperty(...)获取值
		loadPropertyFile("config.properties");
		//设置当前开发模式 如果设置为true 控制台会输出每次请求的Controller action和参数信息
		me.setDevMode(getPropertyToBoolean("devMode"));
		//设置view层根目录
		me.setBaseViewPath("/WEB-INF/view");
		//设置http 404错误跳转页面
		me.setError404View("/WEB-INF/view/error/404.html");
		me.setError500View("/WEB-INF/view/error/500.html");
		//设置Freemarker模板引擎模板文件的后缀名
		me.setFreeMarkerViewExtension(".ftl");
		//设置上传文件默认保存路径根目录
		me.setUploadedFileSaveDirectory(PathKit.getWebRootPath() +"/upload/product");
		//设置URL参数分隔符 http://localhost/demo/blog/1-3  1为文章分类ID 3为分页页码 
		me.setUrlParaSeparator("-");
	}
	
	/**
	 * 配置路由映射
	 */
	@Override	public void configRoute(Routes me) {
		me.add(new AutoBindRoutes());	}
	
	/**
	 * 配置插件
	 */
	@Override
	public void configPlugin(Plugins me) {
		// 配置C3p0数据库连接池插件
		C3p0Plugin c3p0 = new C3p0Plugin(getProperty("jdbcUrl"),
				getProperty("user"), getProperty("password").trim());
		c3p0.setMinPoolSize(40);
		c3p0.setMaxPoolSize(80);
        c3p0.setInitialPoolSize(50);
		me.add(c3p0);
		
		AutoTableBindPlugin atbp = new AutoTableBindPlugin(c3p0,
				SimpleNameStyles.LOWER_UNDERLINE);
		me.add(atbp);
	}
	
	/**
	 * 配置全局拦截器
	 */
	@Override
	public void configInterceptor(Interceptors me) {
		me.add(new LoginInterceptor());
		me.add(new SessionInViewInterceptor());
	}
	
	/**
	 * 配置全局公共处理器
	 */
	@Override
	public void configHandler(Handlers me) {
	}
	
	public static void main(String[] args) {
		JFinal.start("web", 80, "/", 5);
	}

}
