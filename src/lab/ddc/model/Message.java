/**
 * 
 */
package lab.ddc.model;

import com.jfinal.plugin.activerecord.Model;

/**
 * @author zkj
 *
 */
public class Message extends Model<Message> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -560038224145188238L;
	public static final Message dao = new Message();

}
