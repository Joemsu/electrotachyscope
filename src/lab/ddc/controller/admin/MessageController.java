/**
 * 
 */
package lab.ddc.controller.admin;

import java.util.Date;



import lab.ddc.model.Message;
import lab.ddc.util.Modelext;

import com.jfinal.core.Controller;
import com.jfinal.ext.route.ControllerBind;

/**
 * @author zkj
 *
 */
@ControllerBind(controllerKey="/admin/message",viewPath="/admin")
public class MessageController extends Controller {
	
	private static final Modelext modelext = new Modelext("留言","message");
	
	
	public void index(){
		int page = 1;
		if(getParaToInt()!=null){
			page = getParaToInt();
		}
		setAttr("modelext", modelext);
		setAttr("page", Message.dao.paginate(page, 15, "select *","from message" ));
		render("model.ftl");
	}
	
	public void addform(){
		setAttr("modelext", modelext);
		render("model_add.ftl");
	}
	
	public void add(){
		Message message = getModel(Message.class);
		message.set("created", new Date());
		message.save();
		redirect("/admin/message");
	}
	
	public void updateform(){
		setAttr("modelext", modelext);
		setAttr("message", Message.dao.findById(getParaToInt()));
		render("model_update.ftl");
	}
	
	public void update(){
		Message message = getModel(Message.class);
		message.update();
		redirect("/admin/message");
	}
	
	public void delete(){
		Message message = Message.dao.findById(getParaToInt());
		message.delete();
		redirect("/admin/message");
	}

}
