package lab.ddc.util;

import java.util.ArrayList;
import java.util.List;

import com.jfinal.plugin.activerecord.Page;

public class PageUtil {

	public static <T> List<Integer> getPageList(Page<T> page){
		List<Integer> list = new ArrayList<Integer>();
		Integer current = page.getPageNumber();
		Integer total = page.getTotalPage();
		for (int i = current-3; i < current+3; i++) {
			if (i>=1&&i<=total)
				list.add(i);
		}
		return list;
	}
}
