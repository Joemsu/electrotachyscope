package lab.ddc.controller.proscenium;

import lab.ddc.model.Message;

import com.jfinal.core.Controller;
import com.jfinal.ext.render.CaptchaRender;
import com.jfinal.ext.route.ControllerBind;

@ControllerBind(controllerKey="/message",viewPath="/")
public class MessageController extends Controller {
	
	public void index(){
		String inputRandomCode = getPara("randomCode");
        boolean validate = CaptchaRender.validate(this, inputRandomCode, String.valueOf(getSessionAttr("time")));
		System.out.println("validate="+validate);
        if (validate){
			Message message = getModel(Message.class);
			message.save();
			setSessionAttr("message", "已提交成功，请勿再提交！");
			removeSessionAttr("time");
		}else{
			setSessionAttr("message", "验证码错误！");
		}
		redirect("/servicecenter?tab=3");
	}
}
