<#ftl encoding='UTF-8'>
<div class="span9">
	<h1>
		${modelext.getTitle()}管理
		<a href=${baseUrl+"/addform"} class="btn btn-default">新增${modelext.getTitle()}</a>
	</h1>
	<table class="table table-bordered table-striped">
		<thead>
			<tr>
				<th>
					序号
				</th>
				<th>
					公司名称
				</th>
				<th>
					留言人
				</th>
				<th>
					地区
				</th>
				<th>
					QQ
				</th>
				<th>
					联系电话
				</th>
				<th>
					操作
				</th>
			</tr>
		</thead>
		<tbody>
			<#list page.list as league>
				<tr>
					<td>${league_index+1}</td>
					<td>${league.company!}</td>
					<td>${league.person!}</td>
					<td>${league.region!}</td>
					<td>${league.qq!}</td>
					<td>${league.phone!}</td>
					<td>
						<a href="${baseUrl+"/updateform/"+league.id }" class="btn btn-primary">查看详情并更新</a> &nbsp;
						<a href="${baseUrl+"/delete/"+league.id}" class="btn btn-primary">刪除</a>
					</td>
				</tr>

			</#list>
		</tbody>
	</table>		
	<div class="pagination">
		<ul>
			<li>
				<a href=${baseUrl+"/1"}>首页</a>
			</li>
			<#assign last = page.getPageNumber()-1/>
			<#assign current = page.getPageNumber()/>
			<#assign next = page.getPageNumber()+1/>
			<#if last gt 0>
				<li>
					<a href=${baseUrl+"/"+last}>上一页</a>
				</li>
			<#else>
				<li class="disabled">
					<a href="#">上一页</a>
				</li>
			</#if>
			<#list last-2..next+2 as n>
				<#if n gt 0&&n lte page.getTotalPage()>
					<#if n == current>
						<li class="active">
							<a href=${baseUrl+"/"+n}>${n}</a>
						</li>
					<#else>
						<li>
							<a href=${baseUrl+"/"+n}>${n}</a>
						</li>
					</#if>
				</#if>
			</#list>
			<#if next lte page.getTotalPage()>
				<li>
					<a href=${baseUrl+"/"+next}>下一页</a>
				</li>
			<#else>
				<li class="disabled">
					<a href="#">下一页</a>
				</li>
			</#if>
			<li>
				<a href=${baseUrl+"/"+page.getTotalPage()}>末页</a>
			</li>
		</ul>
	</div>
</div>
