package lab.ddc.controller.proscenium;

import java.util.HashMap;
import java.util.Map;

import lab.ddc.model.Article;
import lab.ddc.util.PageUtil;

import com.jfinal.core.Controller;
import com.jfinal.ext.route.ControllerBind;
import com.jfinal.plugin.activerecord.Page;

@ControllerBind(controllerKey="/article")
public class ArticleController extends Controller {

	public void index(){
		Map<String,Object> map = new HashMap<String,Object>();
		Integer type = getParaToInt(0);
		Integer page = getParaToInt(1);
		Page<Article> pages = Article.dao.paginate(page, 12, "select *", "from article where type= "+type+" order by id desc");
		map.put("list", pages.getList());
		map.put("pageList", PageUtil.getPageList(pages));
		renderJson(map);
	}
	
	public void find(){
		Article article = null;
		try {
			article = Article.dao.findById(getParaToLong(0));
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if (article==null) 
				renderJson("null");
			else {
				renderJson(article);
			}
		}
	}
}
