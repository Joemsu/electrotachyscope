package lab.ddc.controller;

import lab.ddc.model.User;
import lab.ddc.util.MD5;

import com.jfinal.core.ActionKey;
import com.jfinal.core.Controller;
import com.jfinal.ext.route.ControllerBind;

@ControllerBind(controllerKey = "/login",viewPath="/admin")
public class LoginController extends Controller {
	
	public void check(){
		String username = getPara("username");
		String password = getPara("password");
		User user = User.dao.findFirst("select * from user where username = '" + username+"'"); 
	    if (user!=null&&user.getInt("enable")==1&&user.getStr("password").equals(MD5.getMD5(password))){
	    	setSessionAttr("name", user.getStr("name"));
	    	render("index.ftl");
	    }else{
	    	redirect("/admin");
	    }
	}
	
	@ActionKey("logout")
	public void logout(){
		removeSessionAttr("name");
    	redirect("/admin");
	}
	
}
