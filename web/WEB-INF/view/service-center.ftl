<!doctype html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge"><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <title>服务中心</title>
    <meta name="description" content="Form. Responsive HTML5 template.">
    <meta name="keywords" content="form, responsive, multi-purpose, portfolio, parallax, template, html5, agency">
    <meta name="author" content="Aether Themes">

    <!--<link rel="shortcut icon" href="img/favicon.png">-->
    <!--<link rel="apple-touch-icon" href="img/apple-touch-icon.png">-->
    <!--<link rel="apple-touch-icon" sizes="72x72" href="img/apple-touch-icon-72x72.png">-->
    <!--<link rel="apple-touch-icon" sizes="114x114" href="img/apple-touch-icon-114x114.png">-->

    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- jQuery -->
    <script src="js/jquery.js" type="text/javascript" charset="utf-8"></script>

    <!-- Stylesheets -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap3.css">
    <link rel="stylesheet" type="text/css" href="css/framework.css">

    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/callus.css">
    <link rel="stylesheet" type="text/css" href="css/job.css">

</head>
<script>
	$(function(){
		<#if tab==3>
			<#if (session.message)??>
				alert("${(session.message)!}");
			</#if>
		</#if>
	})
    function refushcode(){
        var d = new Date();
        document.getElementById("imgId").src="/randomCode?t="+d.toString(40);
    }
</script>
<body>
<!-- Begin Navigation -->

<nav class="desktop">
    <!-- Menu -->
    <ul class="nav-content clearfix">
        <li id="magic-line"></li>
        <li class="first upper">
            <a href="/index">网站首页</a>
        </li>
        <li class="drop upper">
            <a class="drop-btn" href="/about">关于钻豹</a>
            <ul class="drop-list">
                <li><a href="/about?tab=1">企业简介</a></li>
                <li><a href="/about?tab=2">总裁致辞</a></li>
                <li><a href="/about?tab=3">企业文化</a></li>
                <li><a href="/about?tab=4">企业荣誉</a></li>
                <li><a href="/about?tab=5">发展历程</a></li>
            </ul>
        </li>
        <li class="drop upper">
            <a class="drop-btn" href="/question">咨询中心</a>
            <ul class="drop-list">
                <li><a href="/question?tab=1">企业新闻</a></li>
                <li><a href="/question?tab=2">公司大事记</a></li>
                <li><a href="/question?tab=3">行业资讯</a></li>
            </ul>
        </li>
        <li class="upper"><a href="/product">产品中心</a></li>
        <li class="drop upper">
            <a class="drop-btn" href="/brand">品牌营销</a>
            <ul class="drop-list">
                <li><a href="/brand?tab=1">营销网络</a></li>
                <li><a href="/brand?tab=2">加盟钻豹</a></li>
                <li><a href="/brand?tab=3">视频中心</a></li>
                <li><a href="/brand?tab=4">终端形象店</a></li>
                <li><a href="/brand?tab=5">终端活动</a></li>
            </ul>
        </li>
        <li class="current-page drop upper">
            <a class="drop-btn" href="/servicecenter">服务中心</a>
            <ul class="drop-list">
                <li><a href="/servicecenter?tab=1">保养常识</a></li>
                <li><a href="/servicecenter?tab=2">售后服务</a></li>
                <li><a href="/servicecenter?tab=3">客户留言</a></li>
            </ul>
        </li>
        <li class="drop upper">
            <a class="drop-btn" href="/job">招贤纳士</a>
            <ul class="drop-list">
                <li><a href="/job?tab=1">用人理念</a></li>
                <li><a href="/job?tab=2">招聘流程</a></li>
                <li><a href="/job?tab=3">广纳贤才</a></li>
            </ul>
        </li>
        <li class="upper"><a href="/callus">联系钻豹</a></li>
    </ul><!-- END -->

</nav>
<header class="desktop">

    <!-- Logo -->
    <a href="/"><img class="logo" src="img/logo.png" alt="Form Logo" width="96" height="35" /></a>

    <!-- Menu Button -->
    <button type="button" class="nav-button">
        <div class="button-bars"></div>
    </button><!-- END -->

</header>

<div class="sticky-head"></div>
<!-- End Navigation -->
<!-- Begin Large Hero Block -->
<section class="hero accent parallax" ></section>
<!-- End Large Hero Block -->
<div id="container">
    <div id="wrapper">
        <!-- Left Menu Links -->
        <div id="smoothmenu1">
            <ul id="tab-menu" class="ddsmoothmenu" >
                <li <#if tab==1>class="selected"</#if>>保养常识</li>
                <li <#if tab==2>class="selected"</#if>>售后服务</li>
                <li <#if tab==3>class="selected"</#if>>客户留言</li>
            </ul><!-- Introduction Page Starts -->

            <div class="tab-content" style="line-height:30px;text-indent:2em;">
                <div class="tab <#if tab==1>show</#if>">
                    <h1>保养常识</h1>
					${service.baoyang!}
                </div><!-- Introduction Page Ends -->
                <!-- Resume Section Starts -->

                <div class="tab <#if tab==2>show</#if>" style="line-height:30px;">
                    <h1>售后服务</h1><!-- Resume Tab Links -->
  					${service.shouhou!}
                </div><!-- Clients Page Ends -->
                <!-- Page With img Starts -->

                <div class="tab <#if tab==3>show</#if>">
                    <h1>客户留言</h1>

					<div class="m-message">
                        <form role="form" class="form-horizontal" action="/message" method="post">
                            <div class="form-group">
                                <label for="content" class="control-label col-md-3">留言内容：</label>
                                <div class="col-md-9">
                                    <textarea id="content" class="form-control" rows="5" name="message.content" placeholder="请填写您的留言，不得超过500字符" maxlength="500"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="name">留言人：</label>
                                <div class="col-md-9">
                                    <input class="form-control" name="message.name" type="text" id="name" placeholder="请填写你的姓名，小于等于20个字符" value="" maxlength="20"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">性别：</label>
                                <div class="col-md-9">
                                    <label class="radio-inline">
                                        <input type="radio" name="message.sex"  value="0" />
                                        男 </label>

                                    <label class="radio-inline">
                                        <input type="radio" name="message.sex"  value="1" />
                                        女</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="email">您的邮箱：</label>
                                <div class="col-md-9">
                                    <input class="form-control" name="message.name" type="email" id="email" placeholder="示例：example@mail.com" value=""/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="phone">手机号码：</label>
                                <div class="col-md-9">
                                    <input class="form-control" name="message.phone" type="text" id="phone" placeholder="请填写您的手机号码" value="" maxlength="20"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="address">联系方式：</label>
                                <div class="col-md-9">
                                    <input class="form-control" name="message.address" type="text" id="address" placeholder="请填写公司名称" value=""/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="company">公司名称：</label>
                                <div class="col-md-9">
                                    <input class="form-control" name="message.company" type="text" id="company" placeholder="请填写公司名称" value=""/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="companylocation">所在地区：</label>
                                <div class="col-md-9">
                                    <input class="form-control" name="message.companylocation" type="text" id="companylocation" placeholder="请填写您所在的地区" value="" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="code">验证码：</label>
                                <div class="col-md-9">
                                    <input name="randomCode" id="code" size="30" type="text" />
                                    <img src="/randomCode" id="imgId" onclick="refushcode();" />
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-offset-2 col-md-10">
                                    <button type="submit" class="btn btn-primary btn-sm">
                                                                                                            提交
                                    </button>
                                </div>
                            </div>
                        </form>
					</div>
                </div><!-- Page With img Ends -->
                <!-- Services Page Starts -->

            </div>


            <div class="clearfix"></div>
        </div><!-- End -->
        <!-- TWITTER - ADD YOUR USERNAME IN ID INSTEAD OF MINE -->
        <div class="clear"></div>
    </div>
</div>

<!-- Begin Footer -->
<footer>
    <div class="container c-container">
        <div class="row">
            <!-- Social List -->
            <ul class="social-list">
                <li class="copyright">浙江钻豹电动车有限公司</li>
                <li class="copyright">页面版权所有 <a href="#" target="_blank"> 浙ICP备13001925号</a></li>
            </ul><!-- END -->
            <!-- Social List -->
            <ul class="social-list">
                <li class="copyright">WEBPAGE COPYRIGHT (C) 2012 WWW.WXZUANBAO.COM</li>
                <li class="copyright">友情链接： <a href="#" target="_blank"> 电动车</a></li>
                <li class="copyright"> <a href="admin" target="_blank">后台管理</a></li>
                <li class="copyright"> <a href="#" target="_blank"> 技术支持</a></li>
            </ul><!-- END -->
        </div>
    </div>
</footer>
<!-- End Footer -->

<!-- Javascript -->
<script src="js/backgroundcheck.js"></script>
<script src="js/plugins.js"></script>
<script src="js/main.js"></script>
<script type="text/javascript" src="js/scripts.js"></script>
<script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/jquery.validate.min.js"></script>
<script src="js/form.js"></script>
<script type="text/javascript" charset="utf-8">
    MyValidator.init();
</script>
</body>

</html>