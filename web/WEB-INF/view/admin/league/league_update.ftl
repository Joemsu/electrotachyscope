<#ftl encoding='UTF-8'>
<div class="span9">
	<form id="update-form" action=${baseUrl+"/update"} method="post" class="form-horizontal well">
		<fieldset>
			<legend>更新加盟</legend>
			<input type="hidden" id="id" form="update-form" name="league.id" class="span2" value="${league.id!}" />
			<div class="control-group">
				<label class="control-label" for="company">公司名：</label>
				<div class="controls">
					<input type="text" id="company" name="league.company" class="span2" value="${league.company!}" placeholder="请输入公司名" required/>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="person">留言人：</label>
				<div class="controls">
					<input type="text" id="person" name="league.person" class="span2" value="${league.person!}" placeholder="请输入留言人" required/>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="gender">性别</label>
				<div class="controls">
					<select name="league.gender" id="gender" value="${league.gender!}">
						<option value="0">男</option>
						<option value="1">女</option>
					</select>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="age">年龄：</label>
				<div class="controls">
					<input type="number" id="age" name="league.age" value="${league.age!}" class="span2" placeholder="请输入年龄" max="100" min="10" required/>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="region">所在地区：</label>
				<div class="controls">
					<input type="text" id="region" name="league.region" value="${league.region!}" class="span2" placeholder="请输入所在地区" required/>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="qq">QQ：</label>
				<div class="controls">
					<input type="text" id="qq" name="league.qq" class="span2" value="${league.qq!}" placeholder="请输入QQ" required/>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="phone">联系电话：</label>
				<div class="controls">
					<input type="text" id="phone" name="league.phone" class="span2" value="${league.phone!}" placeholder="请输入联系电话" required/>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="email">邮箱：</label>
				<div class="controls">
					<input type="email" id="email" name="league.email" class="span2" value="${league.email!}" placeholder="请输入邮箱" required/>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="fax">传真：</label>
				<div class="controls">
					<input type="text" id="fax" name="league.fax" class="span2" value="${league.fax!}" placeholder="请输入传真"/>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="trade">从事行业：</label>
				<div class="controls">
					<select name="league.trade" id="trade" value="${league.trade!}">
						<option value="0">电动车</option>
						<option value="1">其他</option>
					</select>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="knowable">对电动车了解程度：</label>
				<div class="controls">
					<select name="league.knowable" id="knowable" value="${league.knowable!}">
						<option value="0">非常了解</option>
						<option value="1">比较了解</option>
						<option value="2">一般了解</option>
						<option value="3">不了解</option>
					</select>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="hzintention">合作意向：</label>
				<div class="controls">
					<select name="league.hzintention" id="hzintention" value="${league.hzintention!}">
						<option value="0">专营店（只代理钻豹品牌）</option>
						<option value="1">专营店</option>
					</select>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="area">营业面积：</label>
				<div class="controls">
					<select name="league.area" id="area" value="${league.area!}">
						<option value="0">省会城市</option>
						<option value="1">150平米以上</option>
						<option value="2">地级市</option>
						<option value="3">120平米以上</option>
						<option value="4">县城</option>
						<option value="5">80平米以上</option>
					</select>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="qtintention">洽谈意向：</label>
				<div class="controls">
					<select name="league.qtintention" id="qtintention" value="${league.qtintention!}">
						<option value="0">到钻豹公司洽谈</option>
						<option value="1">区域经理联系您</option>
					</select>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="goodbad">您认为钻豹电动车有什么优势和劣势？：</label>
				<div class="controls">
					<textarea name="league.goodbad" id="goodbad" class="span6" rows="4">${league.goodbad}</textarea>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="brand">您所知道的前5个品牌？：</label>
				<div class="controls">
					<textarea name="league.brand" id="brand" class="span6" rows="4">${league.brand}</textarea>
				</div>
			</div>
			<div class="control-group">
				<div class="controls">
					<input type="submit" class="btn btn-primary" value="提交"/>
				</div>
			</div>	
		</fieldset>
	</form>
</div>