<#ftl encoding='UTF-8'>
<script type="text/javascript" src=${contextPath+"/ueditor/ueditor.config.js"}></script>
<script type="text/javascript" src=${contextPath+"/ueditor/ueditor.all.js"}></script> 
<script type="text/javascript" src=${contextPath+"/ueditor/lang/zh-cn/zh-cn.js"}></script> 
<div class="span9">
	<ul class="nav nav-tabs" id="myTab">
	  <li class="active"><a href="#marketingnet" data-toggle="tab">营销网络</a></li>
	  <li><a href="#video" data-toggle="tab">视频中心</a></li>
	  <li><a href="#imageshop" data-toggle="tab">终端形象店</a></li>
	  <li><a href="#imageactivity" data-toggle="tab">终端活动</a></li>
	</ul>
	<div class="tab-content">
	  <div class="tab-pane active" id="marketingnet">
	  	<form action=${baseUrl+"/add"} method="post" class="form-horizontal well">
	  		<input type="hidden" name="marketing.id" value=${marketing.id}>
			<fieldset>
				<div style="control-group">
					<label class="control-label" for="marketingnet1">内容：</label>
					<div class="controls">
						<script type="text/plain" id="marketingnet1" name="marketing.marketingnet" style="width:100%;height:500px;">${marketing.marketingnet!}</script>
					</div>
				</div>
				<div class="control-group">
					<div class="controls">
						<input type="submit" class="btn btn-primary" value="提交"/>
					</div>
				</div>	
			</fieldset>
		</form>
	  </div>
	  <div class="tab-pane" id="video">
	  	<form action=${baseUrl+"/add"} method="post" class="form-horizontal well">
	  		<input type="hidden" name="marketing.id" value=${marketing.id}>
			<fieldset>
				<div style="control-group">
					<label class="control-label" for="video1">视频地址：</label>
					<div class="controls">
						<input type="text" name="marketing.video" value="${marketing.video!}"class="span6" placeholder="请输入视频地址" required/>
					</div>
				</div>
				<div class="control-group">
					<div class="controls">
						<input type="submit" class="btn btn-primary" value="提交"/>
					</div>
				</div>	
			</fieldset>
		</form>
	  </div>
	  <div class="tab-pane" id="imageshop">
	  	<form action=${baseUrl+"/add"} method="post" class="form-horizontal well">
	  		<input type="hidden" name="marketing.id" value=${marketing.id}>
			<fieldset>
				<div style="control-group">
					<label class="control-label" for="imageshop1">内容：</label>
					<div class="controls">
						<script type="text/plain" id="imageshop1" name="marketing.imageshop" style="width:100%;height:500px;">${marketing.imageshop!}</script>
					</div>
				</div>
				<div class="control-group">
					<div class="controls">
						<input type="submit" class="btn btn-primary" value="提交"/>
					</div>
				</div>	
			</fieldset>
		</form>
	  </div>
	  <div class="tab-pane" id="imageactivity">
	  	<form action=${baseUrl+"/add"} method="post" class="form-horizontal well">
	  	<input type="hidden" name="marketing.id" value=${marketing.id}>
			<fieldset>
				<div style="control-group">
					<label class="control-label" for="imageactivity1">内容：</label>
					<div class="controls">
						<script type="text/plain" id="imageactivity1" name="marketing.imageactivity" style="width:100%;height:500px;">${marketing.imageactivity!}</script>
					</div>
				</div>
				<div class="control-group">
					<div class="controls">
						<input type="submit" class="btn btn-primary" value="提交"/>
					</div>
				</div>	
			</fieldset>
		</form>
	  </div>
	</div>
	
</div>
<script type="text/javascript">
	UE.getEditor('marketingnet1', {allowDivTransToP: false});
	UE.getEditor('video1', {allowDivTransToP: false});
	UE.getEditor('imageshop1', {allowDivTransToP: false});
	UE.getEditor('imageactivity1', {allowDivTransToP: false});
</script>