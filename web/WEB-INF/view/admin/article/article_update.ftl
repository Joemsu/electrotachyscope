<#ftl encoding='UTF-8'>
<script type="text/javascript" src=${contextPath+"/ueditor/ueditor.config.js"}></script>
<script type="text/javascript" src=${contextPath+"/ueditor/ueditor.all.js"}></script> 
<script type="text/javascript" src=${contextPath+"/ueditor/lang/zh-cn/zh-cn.js"}></script> 
<div class="span9">
	<form id="update-form" action=${baseUrl+"/update"} method="post" class="form-horizontal well">
		<fieldset>
			<legend>新增文章</legend>
			<input type="hidden" id="id" form="update-form" name="article.id" class="span2" value="${article.id!}" />
			<div class="control-group">
				<label class="control-label" for="title">标题：</label>
				<div class="controls">
					<input type="text" id="title" name="article.title" class="span2" placeholder="请输入标题" value="${article.title!}" required/>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="author">作者：</label>
				<div class="controls">
					<input type="text" id="author" name="article.author" class="span2" placeholder="请输入作者" value="${article.author!}" required/>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="type">类型：</label>
				<div class="controls">
					<select name = "article.type" id="type" value="${article.type!}">
						<option value = "1">企业新闻</option>
						<option value = "2">公司大事记</option>
						<option value = "3">行业资讯</option>
					</select>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="source">来源：</label>
				<div class="controls">
					<input type="text" id="source" name="article.source" class="span2" placeholder="请输入来源" value="${article.source!}" required/>
				</div>
			</div>
			<div style="control-group">
				<label class="control-label" for="content">内容：</label>
				<div class="controls">
					<script type="text/plain" id="content" name="article.content" style="width:100%;height:200px;">${article.content!}</script>
				</div>
			</div>
			
			<div class="control-group">
				<div class="controls">
					<input type="submit" class="btn btn-primary" value="提交"/>
				</div>
			</div>	
			<script type="text/javascript">
        		UE.getEditor('content', {allowDivTransToP: false});
   			</script>
		</fieldset>
	</form>
</div>