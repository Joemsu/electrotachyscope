<#ftl encoding='UTF-8'>
<div class="span9">
	<form id="update-form" action=${baseUrl+"/update"} method="post" class="form-horizontal well">
		<fieldset>
			<legend>更新用户</legend>
			<input type="hidden" id="id" form="update-form" name="user.id" class="span2" value="${user.id!}" />
			<div class="control-group">
				<label class="control-label" for="username">用户名：</label>
				<div class="controls">
					<input type="text" id="username" name="user.username" class="span2" placeholder="请输入用户名" value="${user.username!}" required/>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="name">昵称：</label>
				<div class="controls">
					<input type="text" id="name" name="user.name" class="span2" placeholder="请输入昵称" value="${user.name!}" required/>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="password">密码：</label>
				<div class="controls">
					<input type="password" id="password" name="user.password" class="span2" placeholder="请输入密码" value="${user.password!}" required/>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="enable">是否可用</label>
				<div class="controls">
					<select name="user.enable" id="enable" value="${user.enable!}">
						<option value="0">不可用</option>
						<option value="1">可用</option>
					</select>
				</div>
			</div>
			<div class="control-group">
				<div class="controls">
					<input type="submit" class="btn btn-primary" value="提交"/>
				</div>
			</div>	
		</fieldset>
	</form>
</div>