<#ftl encoding='UTF-8'>
<#assign contextPath = ""/>
<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge"><![endif]-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<title>首页</title>
	<meta name="description" content="Form. Responsive HTML5 template.">
	<meta name="keywords" content="form, responsive, multi-purpose, portfolio, parallax, template, html5, agency">
	<meta name="author" content="Aether Themes">

	<!--<link rel="shortcut icon" href="img/favicon.png">-->
	<!--<link rel="apple-touch-icon" href="img/apple-touch-icon.png">-->
	<!--<link rel="apple-touch-icon" sizes="72x72" href="img/apple-touch-icon-72x72.png">-->
	<!--<link rel="apple-touch-icon" sizes="114x114" href="img/apple-touch-icon-114x114.png">-->

	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!-- jQuery -->
	<script src="js/jquery.js" type="text/javascript" charset="utf-8"></script>

	<!-- Stylesheets -->
	<link rel="stylesheet" type="text/css" href="css/framework.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">

</head>

<body>
	<!-- Begin Navigation -->

	<nav class="desktop">
    <!-- Menu -->
    <ul class="nav-content clearfix">
        <li id="magic-line"></li>
        <li class="current-page first upper">
            <a href="/index">网站首页</a>
        </li>
        <li class="drop upper">
            <a class="drop-btn" href="/about">关于钻豹</a>
            <ul class="drop-list">
                <li><a href="/about?tab=1">企业简介</a></li>
                <li><a href="/about?tab=2">总裁致辞</a></li>
                <li><a href="/about?tab=3">企业文化</a></li>
                <li><a href="/about?tab=4">企业荣誉</a></li>
                <li><a href="/about?tab=5">发展历程</a></li>
            </ul>
        </li>
        <li class="drop upper">
            <a class="drop-btn" href="/question">咨询中心</a>
            <ul class="drop-list">
                <li><a href="/question?tab=1">企业新闻</a></li>
                <li><a href="/question?tab=2">公司大事记</a></li>
                <li><a href="/question?tab=3">行业资讯</a></li>
            </ul>
        </li>
        <li class="upper"><a href="/product">产品中心</a></li>
        <li class="drop upper">
            <a class="drop-btn" href="/brand">品牌营销</a>
            <ul class="drop-list">
                <li><a href="/brand?tab=1">营销网络</a></li>
                <li><a href="/brand?tab=2">加盟钻豹</a></li>
                <li><a href="/brand?tab=3">视频中心</a></li>
                <li><a href="/brand?tab=4">终端形象店</a></li>
                <li><a href="/brand?tab=5">终端活动</a></li>
            </ul>
        </li>
        <li class="drop upper">
            <a class="drop-btn" href="/servicecenter">服务中心</a>
            <ul class="drop-list">
                <li><a href="/servicecenter?tab=1">保养常识</a></li>
                <li><a href="/servicecenter?tab=2">售后服务</a></li>
                <li><a href="/servicecenter?tab=3">客户留言</a></li>
            </ul>
        </li>
        <li class="drop upper">
            <a class="drop-btn" href="/job">招贤纳士</a>
            <ul class="drop-list">
                <li><a href="/job?tab=1">用人理念</a></li>
                <li><a href="/job?tab=2">招聘流程</a></li>
                <li><a href="/job?tab=3">广纳贤才</a></li>
            </ul>
        </li>
        <li class="upper"><a href="/callus">联系钻豹</a></li>
    </ul><!-- END -->

</nav>
	<header class="desktop">

		<!-- Logo -->
		<a href="/"><img class="logo" src="img/logo.png" alt="Form Logo" width="96" height="35" /></a>

		<!-- Menu Button -->
		<button type="button" class="nav-button">
		  <div class="button-bars"></div>
	    </button><!-- END -->

	</header>

	<div class="sticky-head"></div>
	<!-- End Navigation -->
    <!-- Begin Large Hero Block -->
    <section class="hero accent parallax" ></section>
    <!-- End Large Hero Block -->
<!--begin slider-->
    <div id="home_banner">

        <a id="big_a"><div id="big_img"></div></a>

        <div class="relative">
            <div id="small_img">
                <div class="maxwidth">
                    <div id="small_imgs">
                    	<#list carousels as carousel>
                        	<a href="javascript:;" class="item"><div class="img" b="${carousel.url}" l="" theid="69"> <img src="${carousel.url}"/></div></a>
                        </#list>
                    </div>
                </div>
            </div>
        </div>

    </div>
<!--end slider-->
    <section class="center-mobile content container">
        <div class="row c-introduce">
            <div class="col-sm-4">
                <img class="c-company-img" src="img/jd1.jpg">
            </div>
            <div class="col-sm-4">
                <h4 class="c-introduce-title">企业简介</h4>
                <p class="animated fade">“五星钻豹”切实注重绿色、低碳、节能、环保交通工具的研发。公司一直致力于电动车技术的创新，是集研发、生产、销售为一体的集团性规模化科技型企业，“五星钻豹”通过多年的努力已发展成为电动车行业最具活动品牌。五星钻豹目前已建成四大生产基地：浙江钻豹电动车有限公司、天津钻豹电动车有限公司、东莞钻豹电动车有限公司、河南钻豹电动车有限公司。</p>
            </div>
            <div class="col-sm-4">
                <img class="c-company-img" src="img/jd2.jpg">
            </div>

        </div>
        <div class="row c-introduce">
            <div class="col-sm-4">
                <h4 class="c-introduce-title">企业新闻</h4>
                <ul class="animated fade c-news-list">
                	<#list articles.list as article>
                		<li><a href=${contextPath+"/question?tab=1&id="+article.id}>${article.title!}</a> </li>
                	</#list>
                </ul>
            </div>
            <div class="col-sm-4">
                <img class="c-company-img" src="img/jd1.jpg">
            </div>

            <div class="col-sm-4">
                <img class="c-company-img" src="img/jd2.jpg">
            </div>

        </div>
    </section>
    <!-- End Overview Block -->

    <div class="grid_2">
        <h4 class="animated fade section-title">新车速递</h4>
        <div id="owl-demo3" class="owl-carousel owl-carousel2">
        	<#list products.list as product>
            <div class="item">
                <a href="javascript:void(0)" class="b-link-stroke b-animate-go  thickbox">
                    <img class="lazyOwl" data-src="${product.coverphoto!}" alt="Lazy Owl Image">
                    <div class="b-wrapper">
                        <h2 class="b-animate b-from-left    b-delay03 ">${product.name!}</h2>
                        <p class="b-animate b-from-right    b-delay03 ">
                            ${product.detail!}
                        </p>
                    </div>
                </a>
            </div>
            </#list>
            <div class="clearfix"> </div>
        </div>
    </div>


	<!-- Begin Footer -->
	<footer>
		<div class="container c-container">
			<div class="row">
				<!-- Social List -->
				<ul class="social-list">
					<li class="copyright">浙江钻豹电动车有限公司</li>
					<li class="copyright">页面版权所有 <a href="#" target="_blank"> 浙ICP备13001925号</a></li>
				</ul><!-- END -->
                <!-- Social List -->
                <ul class="social-list">
                    <li class="copyright">WEBPAGE COPYRIGHT (C) 2012 WWW.WXZUANBAO.COM</li>
                    <li class="copyright">友情链接： <a href="#" target="_blank"> 电动车</a></li>
                    <li class="copyright"> <a href="admin" target="_blank">后台管理</a></li>
                    <li class="copyright"> <a href="#" target="_blank"> 技术支持</a></li>
                </ul><!-- END -->
			</div>
		</div>
	</footer>
	<!-- End Footer -->

	<!-- Javascript -->
	<script src="js/backgroundcheck.js"></script>
	<script src="js/plugins.js"></script>
	<script src="js/main.js"></script>
    <script src="js/slider.js"></script>
    <!----- start-Share-instantly-slider---->
    <!-- Prettify -->
    <link href="css/owl.carousel.css" rel="stylesheet">
    <script src="js/owl.carousel.js"></script>
    <script>
        $(document).ready(function() {
            $("#owl-demo , #owl-demo1").owlCarousel({
                items : 1,
                lazyLoad : true,
                autoPlay : true
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            $("#owl-demo3").owlCarousel({
                items : 4,
                lazyLoad : true,
                autoPlay : true,
                navigation: false,
                pagination: false
            });
            
        });
    </script>
    <!----- //End-Share-instantly-slider---->
    <script type="text/javascript" src="js/hover_pack.js"></script>
</body>

</html>