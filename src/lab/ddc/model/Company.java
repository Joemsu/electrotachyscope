package lab.ddc.model;

import com.jfinal.plugin.activerecord.Model;

public class Company extends Model<Company> {
	
	private static final long serialVersionUID = 4056003547764496143L;
	public static final Company dao = new Company();
}
