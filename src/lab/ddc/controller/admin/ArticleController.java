/**
 * 
 */
package lab.ddc.controller.admin;

import lab.ddc.model.Article;
import lab.ddc.util.Modelext;

import com.jfinal.core.Controller;
import com.jfinal.ext.route.ControllerBind;

/**
 * @author zkj
 *
 */
@ControllerBind(controllerKey="/admin/article",viewPath="/admin")
public class ArticleController extends Controller {

	private static final Modelext modelext = new Modelext("文章","article");
	
	public void index(){
		int page = 1;
		if(getParaToInt()!=null){
			page = getParaToInt();
		}
		setAttr("modelext", modelext);
		setAttr("page", Article.dao.paginate(page, 10, "select *", "from article"));
		render("model.ftl");
	}
	
	public void addform(){
		setAttr("modelext", modelext);
		render("model_add.ftl");
	}
	
	public void add(){
		Article article = getModel(Article.class);
		article.save();
		redirect("/admin/article");
	}
	
	public void delete(){
		Article article = Article.dao.findById(getParaToInt());
		article.delete();
		redirect("/admin/article");
		
	}
	
	public void updateform(){
		setAttr("modelext", modelext);
		setAttr("article", Article.dao.findById(getParaToInt()));
		render("model_update.ftl");
	}
	
	public void update(){
		Article article = getModel(Article.class);
		article.update();
		redirect("/admin/article");
	}
	
}
