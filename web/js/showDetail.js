function imghidden(){
    $("#mag").css("visibility","hidden");
}
function imgvisible(){
    $("#mag").css("visibility","visible");
}
function getEventObject(W3CEvent) {            //�¼���׼������
    return W3CEvent || window.event;
}




function getPointerPosition(e) {            //��������������x,y��ú���
    e = e || getEventObject(e);
    var x = e.pageX || (e.clientX + (document.documentElement.scrollLeft || document.body.scrollLeft));
    var y = e.pageY || (e.clientY + (document.documentElement.scrollTop || document.body.scrollTop));
    return { 'x':x,'y':y };
}


function setOpacity(elem,level) {            //�������������͸��ֵ
    if(elem.filters) {
        elem.style.filter = 'alpha(opacity=' + level * 100 + ')';
    } else {
        elem.style.opacity = level;
    }
}


function css(elem,prop) {                //css���ú���,��������cssֵ,���Ҽ�������͸��ֵ
    for(var i in prop) {
        if(i == 'opacity') {
            setOpacity(elem,prop[i]);
        } else {
            elem.style[i] = prop[i];
        }
    }
    return elem;
}


var magnifier = {
    m : null,

    init:function(magni){
        var m = this.m = magni || {
            cont : null,        //װ��ԭʼͼ���div
            img : null,            //�Ŵ��ͼ��
            mag : null,            //�Ŵ��
            scale : 10            //����ֵ,���õ�ֵԽ��Ŵ�Խ��,���������и�����������������ʱ,�����Щ��С�İױ�,Ŀǰ��֪����ν��
        }


        /*
         ָ���Ŵ��ͼƬ�Ĵ�С
         */
        css(m.img,{
            'position' : 'absolute',
            'width' : (360 * m.scale) + 'px',                //ԭʼͼ��Ŀ�*����ֵ    
            'height' : (360 * m.scale) + 'px'                //ԭʼͼ��ĸ�*����ֵ
        })

        /*
         ָ���Ŵ��Ĵ�С
         */
        css(m.mag,{
            'display' : 'none',
            //'width' : (m.cont.clientWidth * m.scale)  + 'px',            //m.contΪԭʼͼ��,��ԭʼͼ��ȿ�
            //'height' : (m.cont.clientHeight * m.scale)  + 'px',
//m.contΪԭʼͼ��,��ԭʼͼ��ȸ�
            'width' : m.cont.clientWidth + 'px',            //m.contΪԭʼͼ��,��ԭʼͼ��ȿ�
            'height' : m.cont.clientHeight + 'px',
            'position' : 'absolute',
            'left' : m.cont.offsetLeft + m.cont.offsetWidth + 10 + 'px',        //�Ŵ���λ��Ϊԭʼͼ����ҷ�Զ10px
            'top' : m.cont.offsetTop + 'px'  //�Ŵ���λ����ԭʼͼ��top��ͬ
        })

        var borderWid = m.cont.getElementsByTagName('div')[0].offsetWidth - m.cont.getElementsByTagName('div')[0].clientWidth;        //��ȡborder�Ŀ�

        css(m.cont.getElementsByTagName('div')[0],{            //m.cont.getElementsByTagName('div')[0]Ϊ�����
            'display' : 'none',                                //��ʼ����Ϊ���ɼ�
            'width' : m.cont.clientWidth / m.scale - borderWid + 'px',            //ԭʼͼƬ�Ŀ�/����ֵ - border�Ŀ��
            'height' : m.cont.clientHeight / m.scale - borderWid + 'px',        //ԭʼͼƬ�ĸ�/����ֵ - border�Ŀ��
            'opacity' : 0.5                    //����͸����
        })

        m.img.src = m.cont.getElementsByTagName('img')[0].src;            //��ԭʼͼ���srcֵ����Ŵ�ͼ��
        m.cont.style.cursor = 'crosshair';  //����ʱ���Ϊʮ����

        m.cont.onmouseover = magnifier.start;

    },
    /*
     ������ͼƬʱ����
     */
    start:function(e){
        if(document.all){                //ֻ��IE��ִ��,��Ҫ����IE6��select�޷�����
//����һ��iframe�����Ŵ��img�Ž�ȥ
            magnifier.createIframe(magnifier.m.img);
        }

        this.onmousemove = magnifier.move;        //thisָ��m.cont
        //this.onmouseout = magnifier.end;
    },
    /*
     ����ƶ�ʱ����
     */
    move:function(e){
        var pos = getPointerPosition(e);        //�¼���׼��

        this.getElementsByTagName('div')[0].style.display = '';

//this.offsetLeft:250px.ԭʼͼƬ����div��left
//this.getElementsByTagName('div')[0].style.width:112




//this.clientWidth:ԭʼͼƬ:342
//this.getElementsByTagName('div')[0].offsetWidth:114/0

//this.getElementsByTagName('div')[0].id:browser


//ȷ��broswer����ʾ��Χ



        css(this.getElementsByTagName('div')[0],{
            'top' : Math.min(Math.max(pos.y - this.offsetTop - parseInt(this.getElementsByTagName('div')[0].style.height) / 2,0),this.clientHeight - this.getElementsByTagName('div')[0].offsetHeight) + 'px',
            'left' : Math.min(Math.max(pos.x - this.offsetLeft - parseInt(this.getElementsByTagName('div')[0].style.width) / 2,0),this.clientWidth - this.getElementsByTagName('div')[0].offsetWidth) + 'px'
//�������ͼƬ�й���λ��ʱ������Math.max������   ���ڹ���λ��ʱ������ Math.min������
        })



//��ʾ���ڿ�
        magnifier.m.mag.style.display = '';



        /*
         �ı��Ҳ�ͼ����ʾ
         */
        css(magnifier.m.img,{
            'top' : - (parseInt(this.getElementsByTagName('div')[0].style.top) * magnifier.m.scale) + 'px',
            'left' : - (parseInt(this.getElementsByTagName('div')[0].style.left) * magnifier.m.scale) + 'px'
            //'left':'-200px',  //ͼƬ�����ƶ�200px
//'top':'500px'
        })



    },
    /*
     ����뿪ʱ����
     */
    end:function(e){
        this.getElementsByTagName('div')[0].style.display = 'none';//div Browser   //������ڵĺڿ�
        magnifier.removeIframe(magnifier.m.img);        //���iframe
        magnifier.m.mag.style.display = 'none';   //div mag //�Ŵ��ͼƬ
    },

    /*
     ������ʱ����
     */
    createIframe:function(elem){
        var layer = document.createElement('iframe');
        layer.tabIndex = '-1';
        layer.src = 'javascript:false;';
        elem.parentNode.appendChild(layer);

        layer.style.width = elem.offsetWidth + 'px';
        layer.style.height = elem.offsetHeight + 'px';
    },
    /*
     ����뿪ʱ����
     */
    removeIframe:function(elem){
        var layers = elem.parentNode.getElementsByTagName('iframe');
        while(layers.length >0){
            layers[0].parentNode.removeChild(layers[0]);
        }
    }
}


window.onload = function(){
    magnifier.init({
        cont : document.getElementById('magnifier'),
        img : document.getElementById('magnifierImg'),
        mag : document.getElementById('mag'),
        scale : 3
    });
}