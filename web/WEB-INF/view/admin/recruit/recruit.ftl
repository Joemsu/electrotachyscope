<#ftl encoding='UTF-8'>
<script type="text/javascript" src=${contextPath+"/ueditor/ueditor.config.js"}></script>
<script type="text/javascript" src=${contextPath+"/ueditor/ueditor.all.js"}></script> 
<script type="text/javascript" src=${contextPath+"/ueditor/lang/zh-cn/zh-cn.js"}></script> 
<div class="span9">
	<ul class="nav nav-tabs" id="myTab">
	  <li class="active"><a href="#intro" data-toggle="tab">用人理念</a></li>
	  <li><a href="#oration" data-toggle="tab">招聘流程</a></li>
	  <li><a href="#wide" data-toggle="tab">广纳贤才</a></li>
	</ul>
	<div class="tab-content">
	  <div class="tab-pane active" id="intro">
	  	<form action=${baseUrl+"/add"} method="post" class="form-horizontal well">
	  		<input type="hidden" name="recruit.id" value=${recruit.id}>
			<fieldset>
				<div style="control-group">
					<label class="control-label" for="intro1">内容：</label>
					<div class="controls">
						<script type="text/plain" id="intro1" name="recruit.useperson" style="width:100%;height:500px;">${recruit.useperson!}</script>
					</div>
				</div>
				<div class="control-group">
					<div class="controls">
						<input type="submit" class="btn btn-primary" value="提交"/>
					</div>
				</div>	
			</fieldset>
		</form>
	  </div>
	  <div class="tab-pane" id="oration">
	  	<form action=${baseUrl+"/add"} method="post" class="form-horizontal well">
	  		<input type="hidden" name="recruit.id" value=${recruit.id}>
			<fieldset>
				<div style="control-group">
					<label class="control-label" for="oration1">内容：</label>
					<div class="controls">
						<script type="text/plain" id="oration1" name="recruit.zp" style="width:100%;height:500px;">${recruit.zp!}</script>
					</div>
				</div>
				<div class="control-group">
					<div class="controls">
						<input type="submit" class="btn btn-primary" value="提交"/>
					</div>
				</div>	
			</fieldset>
		</form>
	  </div>
	  <div class="tab-pane" id="wide">
	  	<form action=${baseUrl+"/add"} method="post" class="form-horizontal well">
	  		<input type="hidden" name="recruit.id" value=${recruit.id}>
			<fieldset>
				<div style="control-group">
					<label class="control-label" for="wide1">内容：</label>
					<div class="controls">
						<script type="text/plain" id="wide1" name="recruit.widexc" style="width:100%;height:500px;">${recruit.widexc!}</script>
					</div>
				</div>
				<div class="control-group">
					<div class="controls">
						<input type="submit" class="btn btn-primary" value="提交"/>
					</div>
				</div>	
			</fieldset>
		</form>
	  </div>
	</div>
	
</div>
<script type="text/javascript">
	UE.getEditor('intro1', {allowDivTransToP: false});
	UE.getEditor('oration1', {allowDivTransToP: false});
	UE.getEditor('wide1', {allowDivTransToP: false});
</script>