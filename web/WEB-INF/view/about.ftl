<#ftl encoding='UTF-8'>
<#assign contextPath = ""/>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge"><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <title>关于钻豹</title>
    <meta name="description" content="Form. Responsive HTML5 template.">
    <meta name="keywords" content="form, responsive, multi-purpose, portfolio, parallax, template, html5, agency">
    <meta name="author" content="Aether Themes">

    <!--<link rel="shortcut icon" href="img/favicon.png">-->
    <!--<link rel="apple-touch-icon" href="img/apple-touch-icon.png">-->
    <!--<link rel="apple-touch-icon" sizes="72x72" href="img/apple-touch-icon-72x72.png">-->
    <!--<link rel="apple-touch-icon" sizes="114x114" href="img/apple-touch-icon-114x114.png">-->

    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- jQuery -->
    <script src="js/jquery.js" type="text/javascript" charset="utf-8"></script>

    <!-- Stylesheets -->
    <link rel="stylesheet" type="text/css" href="css/framework.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/job.css">
    <link rel="stylesheet" type="text/css" href="css/about.css">
    <link rel="stylesheet" type="text/css" href="css/waterfall.css" media='screen' >
</head>

<body>
<!-- Begin Navigation -->

<nav class="desktop">
    <!-- Menu -->
    <ul class="nav-content clearfix">
        <li id="magic-line"></li>
        <li class="first upper">
            <a href="/index">网站首页</a>
        </li>
        <li class="current-page drop upper">
            <a class="drop-btn" href="/about">关于钻豹</a>
            <ul class="drop-list">
                <li><a href="/about?tab=1">企业简介</a></li>
                <li><a href="/about?tab=2">总裁致辞</a></li>
                <li><a href="/about?tab=3">企业文化</a></li>
                <li><a href="/about?tab=4">企业荣誉</a></li>
                <li><a href="/about?tab=5">发展历程</a></li>
            </ul>
        </li>
        <li class="drop upper">
            <a class="drop-btn" href="/question">咨询中心</a>
            <ul class="drop-list">
                <li><a href="/question?tab=1">企业新闻</a></li>
                <li><a href="/question?tab=2">公司大事记</a></li>
                <li><a href="/question?tab=3">行业资讯</a></li>
            </ul>
        </li>
        <li class="upper"><a href="/product">产品中心</a></li>
        <li class="drop upper">
            <a class="drop-btn" href="/brand">品牌营销</a>
            <ul class="drop-list">
                <li><a href="/brand?tab=1">营销网络</a></li>
                <li><a href="/brand?tab=2">加盟钻豹</a></li>
                <li><a href="/brand?tab=3">视频中心</a></li>
                <li><a href="/brand?tab=4">终端形象店</a></li>
                <li><a href="/brand?tab=5">终端活动</a></li>
            </ul>
        </li>
        <li class="drop upper">
            <a class="drop-btn" href="/servicecenter">服务中心</a>
            <ul class="drop-list">
                <li><a href="/servicecenter?tab=1">保养常识</a></li>
                <li><a href="/servicecenter?tab=2">售后服务</a></li>
                <li><a href="/servicecenter?tab=3">客户留言</a></li>
            </ul>
        </li>
        <li class="drop upper">
            <a class="drop-btn" href="/job">招贤纳士</a>
            <ul class="drop-list">
                <li><a href="/job?tab=1">用人理念</a></li>
                <li><a href="/job?tab=2">招聘流程</a></li>
                <li><a href="/job?tab=3">广纳贤才</a></li>
            </ul>
        </li>
        <li class="upper"><a href="/callus">联系钻豹</a></li>
    </ul><!-- END -->

</nav>
<header class="desktop">

    <!-- Logo -->
    <a href="/"><img class="logo" src="img/logo.png" alt="Form Logo" width="96" height="35" /></a>

    <!-- Menu Button -->
    <button type="button" class="nav-button">
        <div class="button-bars"></div>
    </button><!-- END -->

</header>

<div class="sticky-head"></div>
<!-- End Navigation -->
<!-- Begin Large Hero Block -->
<section class="hero accent parallax" ></section>
<!-- End Large Hero Block -->
<div id="container">
    <div id="wrapper">
        <!-- Left Menu Links -->
        <div id="smoothmenu1">
            <ul id="tab-menu" class="ddsmoothmenu c-introduction-nav" >
                <li <#if tab==1>class="selected"</#if>>企业简介</li>
                <li <#if tab==2>class="selected"</#if>>总裁致辞</li>
                <li <#if tab==3>class="selected"</#if>>企业文化</li>
                <li class="c-reward <#if tab==4>selected</#if>">企业荣耀</li>
                <li <#if tab==5>class="selected"</#if>>发展历程</li>
            </ul><!-- Introduction Page Starts -->

            <div class="tab-content">
                <div class="tab <#if tab==1>show</#if>" >
                    <h1>企业简介</h1>
                    ${company.intro!}
                    <div id="latest-blog">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="heading-section">
                                        <h2>四大生产基地</h2>
                                        <img src="img/under-heading.png" alt="" >
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-sm-12">
                                    <div class="blog-post">
                                        <div class="blog-thumb">
                                            <img src="img/jd1.jpg" alt="" />
                                        </div>
                                        <div class="blog-content">
                                            <div class="content-show">
                                                <h4>浙江五星钻豹生产基地</h4>
                                            </div>
                                            <div class="content-hide">
                                                <p>浙江钻豹电动车有限公司，座落在美丽的东海之滨浙江台州经济开发区，拥有大型现代化生产厂房6万多平方米，配备自动化生产悬挂流水线5条及先进的研发、检测设备，主要生产、销售豪华款车型，年生产能力达60万辆，现有员工1000余人，专科以上文化程度的专业型管理人才150余人，其中工程师、高级工程师20余名。2013年公司全面推动企业发展，目前厂区设有1万平方米烤漆部，年生产烤漆件40万套；已建成5条现代化同步成车装配生产线和2条成车包装、入库、发运输送线，有效保障企业产品制造与销售。</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <div class="blog-post">
                                        <div class="blog-thumb">
                                            <img src="img/jd2.jpg" alt="" />
                                        </div>
                                        <div class="blog-content">
                                            <div class="content-show">
                                                <h4>天津五星钻豹生产基地</h4>
                                            </div>
                                            <div class="content-hide">
                                                <p>天津钻豹电动车有限公司位于天津市北辰区，现拥有现代化生产厂房3万平方米，现有员工450人，其中大专以上文化程度专业型管理人才100余人，主要以生产简易款车型为主，年生产能力达50万辆，公司设有烤漆部，年生产烤漆件30万套，以提升市场对车型外观竞争力。</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <div class="blog-post">
                                        <div class="blog-thumb">
                                            <img src="img/jd3.jpg" alt="" />
                                        </div>
                                        <div class="blog-content">
                                            <div class="content-show">
                                                <h4>河南五星钻豹生产基地</h4>
                                            </div>
                                            <div class="content-hide">
                                                <p>河南钻豹电动车有限公司位于河南通许县，拥有200亩生产基地，大型厂房8万平方米，主要研发、生产、销售三轮车为主，年生产能力达30万台。</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <div class="blog-post">
                                        <div class="blog-thumb">
                                            <img src="img/jd4.jpg" alt="" />
                                        </div>
                                        <div class="blog-content">
                                            <div class="content-show">
                                                <h4>广东东莞五星钻豹生产基地</h4>
                                            </div>
                                            <div class="content-hide">
                                                <p>东莞钻豹电动车有限公司，拥有现代化厂房45000平方，自2012年底成立，现主要研发、生产、销售载重全系列和豪华款为主，年生产能力达30万台。</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <div class="heading-section">
                        <img src="img/under-heading.png" alt="" >
                         <p class="introduction-content">公司的销售网点遍布全国，已经拥有4000多个营销网点和售后服务网点，产品受到广大用户一致好评。</p>
                        <p class="introduction-content">五星钻豹品牌电动车一直坚持以“差异化”战略思想，本着“敢为天下先”的经营理念，依靠专业的研发技术、雄厚的经济实力，不断开发新产品、新卖点，提升产品技术、品质，努力做到“人无我有，人有我优，人优我全，人全我异。五星钻豹2008年被授于市场“放心质量,售后服务满意”荣誉称号，公司严格按照ISO9000国际质量管理体系进行生产和质量控制；五星钻豹全力打造中国驰名商标，为节能创新发展而不懈追求。</p>         
                    </div>
                </div>
                <div class="tab <#if tab==2>show</#if>">
                    <h1>总裁致辞</h1><!-- Resume Tab Links -->
                    ${company.oration!}
                </div>
                <div class="tab <#if tab==3>show</#if>">
                    <h1>企业文化</h1><!-- Resume Tab Links -->
                    ${company.culture!}
                </div>
                <div class="tab masonry-tab <#if tab==4>show</#if>">
                    <h1>企业荣誉</h1><!-- Resume Tab Links -->
                    ${company.glory!}
                </div>
                <div class="tab <#if tab==5>show</#if>">
                    <h1>发展历程</h1><!-- Resume Tab Links -->
                    ${company.history!}
                </div>
            </div>


            <div class="clearfix"></div>
        </div><!-- End -->
        <!-- TWITTER - ADD YOUR USERNAME IN ID INSTEAD OF MINE -->
        <div class="clear"></div>
    </div>
</div>

<!-- Begin Footer -->
<footer>
    <div class="container c-container">
        <div class="row">
            <!-- Social List -->
            <ul class="social-list">
                <li class="copyright">浙江钻豹电动车有限公司</li>
                <li class="copyright">页面版权所有 <a href="#" target="_blank"> 浙ICP备13001925号</a></li>
            </ul><!-- END -->
            <!-- Social List -->
            <ul class="social-list">
                <li class="copyright">WEBPAGE COPYRIGHT (C) 2012 WWW.WXZUANBAO.COM</li>
                <li class="copyright">友情链接： <a href="#" target="_blank"> 电动车</a></li>
                <li class="copyright"> <a href="admin" target="_blank">后台管理</a></li>
                <li class="copyright"> <a href="#" target="_blank"> 技术支持</a></li>
            </ul><!-- END -->
        </div>
    </div>
</footer>
<!-- End Footer -->

<!-- Javascript -->
<script src="js/backgroundcheck.js"></script>
<script src="js/plugins.js"></script>

<script src="js/main.js"></script>
<script type="text/javascript" src="js/scripts.js"></script>
<script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>
<script src="js/introduction-list.js"></script>
<script src="js/blocksit.min.js"></script>
<script src="js/warterfall.js"></script>
</body>

</html>