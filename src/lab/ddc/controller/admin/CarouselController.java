package lab.ddc.controller.admin;

import lab.ddc.model.Carousel;
import lab.ddc.util.Modelext;

import com.jfinal.core.Controller;
import com.jfinal.ext.route.ControllerBind;
import com.jfinal.upload.UploadFile;

@ControllerBind(controllerKey="/admin/carousel",viewPath="/admin")
public class CarouselController extends Controller {
	
	private static final Modelext modelext = new Modelext("轮播图","carousel");
	
	public void index(){
		int page = 1;
		if(getParaToInt()!=null){
			page = getParaToInt();
		}
		setAttr("modelext", modelext);
		setAttr("page", Carousel.dao.paginate(page, 10, "select *", "from carousel"));
		render("model.ftl");
	}
	
	public void addform(){
		setAttr("modelext", modelext);
		render("model_add.ftl");
	}
	
	public void add(){
		UploadFile file=getFile("img");	
		Carousel carousel = new Carousel();
		carousel.set("url", "http://120.26.82.151:8080/ddc/upload/product/"+file.getFileName());
		carousel.save();
		redirect("/admin/carousel");
	}
	
	public void delete(){
		Carousel carousel = Carousel.dao.findById(getParaToInt());
		carousel.delete();
		redirect("/admin/carousel");
		
	}
	
}
