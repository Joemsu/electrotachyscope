<#ftl encoding='UTF-8'>
<div class="span9">
	<form id="update-form" action=${baseUrl+"/update"} method="post" class="form-horizontal well">
		<fieldset>
			<legend>新增留言</legend>
			<input type="hidden" id="id" form="update-form" name="message.id" class="span2" value="${message.id!}" />
			<div class="control-group">
				<label class="control-label" for="name">留言人：</label>
				<div class="controls">
					<input type="text" id="name" name="message.name" class="span2" value="${message.name}" placeholder="请输入留言人" required/>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="sex">性别：</label>
				<div class="controls">
					<#if message.sex == 1>
					<input type= "radio" value="1" id="sex1" name="message.sex" checked="checked">男
					<input type= "radio" value="0" id="sex0" name="message.sex" >女
					<#elseif message.sex ==0>
					<input type= "radio" value="1" id="sex1" name="message.sex" >男
					<input type= "radio" value="0" id="sex0" name="message.sex" checked="checked">女
					</#if>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="email">邮箱：</label>
				<div class="controls">
					<input type="text" id="email" name="message.email" class="span2" value="${message.email}" placeholder="请输入邮箱" required/>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="phone">联系电话：</label>
				<div class="controls">
					<input type="text" id="phone" name="message.phone" class="span2" value="${message.phone}" placeholder="请输入联系电话" required/>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="company">公司名称：</label>
				<div class="controls">
					<input type="text" id="company" name="message.company" class="span2" value="${message.company}" placeholder="请输入公司名称" required/>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="companylocation">公司所在地区：</label>
				<div class="controls">
					<input type="text" id="companylocation" name="message.companylocation" class="span2" value="${message.companylocation}" placeholder="请输入公司所在地区" required/>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="address">联系地址：</label>
				<div class="controls">
					<input type="text" id="address" name="message.address" class="span2" value="${message.address}" placeholder="请输入联系地址" required/>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="content">内容：</label>
				<div class="controls">
					<textarea id="content" name="message.content" class="span6" rows="5" required>${message.content}</textarea>
				</div>
			</div>
			<div class="control-group">
				<div class="controls">
					<input type="submit" class="btn btn-primary" value="提交"/>
				</div>
			</div>	
		</fieldset>
	</form>
</div>