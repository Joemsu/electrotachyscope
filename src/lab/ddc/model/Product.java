/**
 * 
 */
package lab.ddc.model;

import com.jfinal.plugin.activerecord.Model;

/**
 * @author zkj
 *
 */
public class Product extends Model<Product> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2763403869974037888L;
	public static final Product dao = new Product();

}
