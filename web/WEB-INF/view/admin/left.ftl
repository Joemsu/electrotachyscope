<#ftl encoding='UTF-8'>
<div class="span3">
    <div class="well" style="padding: 8px 0;">
        <ul class="nav nav-list">
            <li class="nav-header">
              	  电动车
            </li>
            <li class="active">
                <a href="/admin"><i class="icon-white icon-home"></i> 首页</a>
            </li>
            <li>
                <a href=${contextPath+"/admin/user"}><i class="icon-user"></i> 用户管理</a>
            </li>
            <li>
                <a href=${contextPath+"/admin/article"}><i class="icon-book"></i> 文章管理</a>
            </li>
            <li>
                <a href=${contextPath+"/admin/product"}><i class="icon-gift"></i> 产品管理</a>
            </li>
            <li>
                <a href=${contextPath+"/admin/message"}><i class="icon-comment"></i> 留言管理</a>
            </li>
            <li>
                <a href=${contextPath+"/admin/league"}><i class="icon-star"></i> 加盟管理</a>
            </li>
            <li>
                <a href=${contextPath+"/admin/carousel"}><i class="icon-picture"></i> 轮播图管理</a>
            </li>
            <li>
                <a href=${contextPath+"/admin/company"}><i class="icon-qrcode"></i> 公司简介管理</a>
            </li>
            <li>
                <a href=${contextPath+"/admin/service"}><i class="icon-qrcode"></i> 服务中心管理</a>
            </li>
            <li>
                <a href=${contextPath+"/admin/marketing"}><i class="icon-qrcode"></i> 品牌营销管理</a>
            </li>
            <li>
                <a href=${contextPath+"/admin/recruit"}><i class="icon-qrcode"></i> 招贤纳士管理</a>
            </li>
            <li class="nav-header">
                	你的账户
            </li>
            <li>
                <a href="#"><i class="icon-user"></i> 简介</a>
            </li>
            <li>
                <a href="#"><i class="icon-cog"></i> 设置</a>
            </li>
            <li class="divider">
            </li>
            <li>
                <a href="#"><i class="icon-info-sign"></i> 帮助</a>
            </li>
        </ul>
    </div>
</div>
