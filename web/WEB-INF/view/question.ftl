<#ftl encoding='UTF-8'>
<#assign contextPath = ""/>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge"><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <title>服务中心</title>
    <meta name="description" content="Form. Responsive HTML5 template.">
    <meta name="keywords" content="form, responsive, multi-purpose, portfolio, parallax, template, html5, agency">
    <meta name="author" content="Aether Themes">

    <!--<link rel="shortcut icon" href="img/favicon.png">-->
    <!--<link rel="apple-touch-icon" href="img/apple-touch-icon.png">-->
    <!--<link rel="apple-touch-icon" sizes="72x72" href="img/apple-touch-icon-72x72.png">-->
    <!--<link rel="apple-touch-icon" sizes="114x114" href="img/apple-touch-icon-114x114.png">-->

    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- jQuery -->
    <script src="js/jquery.js" type="text/javascript" charset="utf-8"></script>

    <!-- Stylesheets -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap3.css">
    <link rel="stylesheet" type="text/css" href="css/framework.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/job.css">
    <link rel="stylesheet" type="text/css" href="css/callus.css">
</head>
<script>
	var type=${tab!},curpage=1,h1="企业新闻",id=${id?default(0)};
	var getJson = function(newtype,newpage){
					type =newtype;
					curpage = newpage;
					if(type===1)
						h1="企业新闻";
					else if (type===2)
						h1="公司大事记";
					else h1="行业资讯";
				　　$.get("${contextPath}/article/"+type+"-"+curpage,
						function(data){
			                var tpl=new t($("#tpl").html());
							$(".m-question").html(tpl.render({"data":data,"type":type,"page":curpage,"h1":h1}));
						});
					}
	var getJsonById = function(id,i){
					id=id+i;
				　　$.get("${contextPath}/article/find/"+id,
						function(data){
							if(data===null)
								alert("文章不存在!");
							else{
				                var oneresult=new t($("#oneresult").html());
								$(".m-question").html(oneresult.render({"data":data,"h1":h1}));
							}
						});
					}
	if(id===0)
		getJson(type,curpage);
	else
		getJsonById(id,0);
</script>
<body>
<!-- Begin Navigation -->

<nav class="desktop">
    <!-- Menu -->
    <ul class="nav-content clearfix">
        <li id="magic-line"></li>
        <li class="first upper">
            <a href="/index">网站首页</a>
        </li>
        <li class="drop upper">
            <a class="drop-btn" href="/about">关于钻豹</a>
            <ul class="drop-list">
                <li><a href="/about?tab=1">企业简介</a></li>
                <li><a href="/about?tab=2">总裁致辞</a></li>
                <li><a href="/about?tab=3">企业文化</a></li>
                <li><a href="/about?tab=4">企业荣誉</a></li>
                <li><a href="/about?tab=5">发展历程</a></li>
            </ul>
        </li>
        <li class="current-page drop upper">
            <a class="drop-btn" href="/question">咨询中心</a>
            <ul class="drop-list">
                <li><a href="/question?tab=1">企业新闻</a></li>
                <li><a href="/question?tab=2">公司大事记</a></li>
                <li><a href="/question?tab=3">行业资讯</a></li>
            </ul>
        </li>
        <li class="upper"><a href="/product">产品中心</a></li>
        <li class="drop upper">
            <a class="drop-btn" href="/brand">品牌营销</a>
            <ul class="drop-list">
                <li><a href="/brand?tab=1">营销网络</a></li>
                <li><a href="/brand?tab=2">加盟钻豹</a></li>
                <li><a href="/brand?tab=3">视频中心</a></li>
                <li><a href="/brand?tab=4">终端形象店</a></li>
                <li><a href="/brand?tab=5">终端活动</a></li>
            </ul>
        </li>
        <li class="drop upper">
            <a class="drop-btn" href="/servicecenter">服务中心</a>
            <ul class="drop-list">
                <li><a href="/servicecenter?tab=1">保养常识</a></li>
                <li><a href="/servicecenter?tab=2">售后服务</a></li>
                <li><a href="/servicecenter?tab=3">客户留言</a></li>
            </ul>
        </li>
        <li class="drop upper">
            <a class="drop-btn" href="/job">招贤纳士</a>
            <ul class="drop-list">
                <li><a href="/job?tab=1">用人理念</a></li>
                <li><a href="/job?tab=2">招聘流程</a></li>
                <li><a href="/job?tab=3">广纳贤才</a></li>
            </ul>
        </li>
        <li class="upper"><a href="/callus">联系钻豹</a></li>
    </ul><!-- END -->

</nav>
<header class="desktop">

    <!-- Logo -->
    <a href="/"><img class="logo" src="img/logo.png" alt="Form Logo" width="96" height="35" /></a>

    <!-- Menu Button -->
    <button type="button" class="nav-button">
        <div class="button-bars"></div>
    </button><!-- END -->

</header>

<div class="sticky-head"></div>
<!-- End Navigation -->
<!-- Begin Large Hero Block -->
<section class="hero accent parallax" ></section>
<!-- End Large Hero Block -->
<div id="container">
    <div id="wrapper">
        <!-- Left Menu Links -->
        <div id="smoothmenu1">
            <ul id="tab-menu" class="ddsmoothmenu" >
                <li <#if tab==1>class="selected"</#if> onclick="getJson(1,1)">企业新闻</li>
                <li <#if tab==2>class="selected"</#if> onclick="getJson(2,1)">公司大事记</li>
                <li <#if tab==3>class="selected"</#if> onclick="getJson(3,1)">行业资讯</li>
            </ul>

            <div class="tab-content m-question"></div>

            <div class="clearfix"></div>
        </div><!-- End -->
        <!-- TWITTER - ADD YOUR USERNAME IN ID INSTEAD OF MINE -->
        <div class="clear"></div>
    </div>
</div>

<!-- Begin Footer -->
<footer>
    <div class="container c-container">
        <div class="row">
            <!-- Social List -->
            <ul class="social-list">
                <li class="copyright">浙江钻豹电动车有限公司</li>
                <li class="copyright">页面版权所有 <a href="#" target="_blank"> 浙ICP备13001925号</a></li>
            </ul><!-- END -->
            <!-- Social List -->
            <ul class="social-list">
                <li class="copyright">WEBPAGE COPYRIGHT (C) 2012 WWW.WXZUANBAO.COM</li>
                <li class="copyright">友情链接： <a href="#" target="_blank"> 电动车</a></li>
                <li class="copyright"> <a href="admin" target="_blank">后台管理</a></li>
                <li class="copyright"> <a href="#" target="_blank"> 技术支持</a></li>
            </ul><!-- END -->
        </div>
    </div>
</footer>
<!-- End Footer -->

<!-- Javascript -->
<script src="js/backgroundcheck.js"></script>
<script src="js/plugins.js"></script>
<script src="js/main.js"></script>
<script type="text/javascript" src="js/scripts.js"></script>
<script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>
</body>
<script type="t/template" id="tpl">
<div class="tab show ">
	<h1>
		{{=h1}}
	</h1>
	<ul class="m-que-ul">
		{{@data.list}}
		<li>
			<a href=javascript:getJsonById({{=_val.id}},0)><span>{{=_val.title}}</span><span class="t-list2">{{=_val.created}}</span></a>
	    </li>
	     
	    {{/@data.list}}
	</ul>
	<nav>
		<ul class="pagination m-nav pagination-sm">
			{{@data.pageList}}
		    	<li><a href=javascript:getJson({{=type}},{{=_val}})>{{=_val}}</a></li>
		    {{/@data.pageList}}
		</ul>
	</nav>
</div>
</script> 
<script type="t/template" id="oneresult">
<div class="tab show ">
	<h1>
		{{=data.title}}
	</h1>
	<p class="article-sourse">
	作者：{{=data.author}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	来源：{{=data.source}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	发布日期：{{=data.created}}
	</p>
	<ul class="m-que-ul">
		{{=data.content}}
	</ul>
	<a href=javascript:getJsonById({{=data.id}},-1)>上一篇</a>
	<a href=javascript:getJsonById({{=data.id}},1)>下一篇</a>
	<p>所属类别:{{=h1}}</p>
</div>
</script> 
</html>